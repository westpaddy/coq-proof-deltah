Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From DH Require Export DH_Infrastructure.

(** ** subset relations on syntax categories *)

Lemma pnvalue_is_pvalue : forall nv,
    pnvalue nv ->
    pvalue nv.
Proof. induction 1; auto. Qed.

Lemma pnvalue_is_pexp : forall nv,
    pnvalue nv ->
    pexp nv.
Proof. induction 1; auto. Qed.

Lemma pvalue_is_pexp : forall v,
    pvalue v ->
    pexp v.
Proof. induction 1; auto using pnvalue_is_pexp. Qed.

Lemma interf_is_typ : forall I,
    interf I ->
    typ I.
Proof. induction 1; auto. Qed.

Lemma rbody_is_exp : forall B,
    rbody B ->
    exp B.
Proof. induction 1; auto. Qed.

Lemma nvalue_is_value : forall nv,
    nvalue nv ->
    value nv.
Proof. induction 1; auto. Qed.

Lemma nvalue_is_exp : forall nv,
    nvalue nv ->
    exp nv.
Proof. induction 1; auto. Qed.

Lemma value_is_exp : forall v,
    value v ->
    exp v.
Proof. induction 1; auto using nvalue_is_exp. Qed.

Lemma exp_is_command : forall e,
    exp e ->
    command e.
Proof. unfold command. auto. Qed.

Hint Resolve pnvalue_is_pvalue pnvalue_is_pexp pvalue_is_pexp
     interf_is_typ rbody_is_exp nvalue_is_value nvalue_is_exp value_is_exp exp_is_command.

(** * well-definedness of definitions *)

(** ** substitution is closed in syntax categories *)

Section PCFv.
Open Scope pcf_scope.

Lemma subst_ptyp : forall x u T,
    ptyp T ->
    pexp u ->
    ptyp ([x ~> u]T).
Proof. induction 1; simpl; auto. Qed.

Lemma subst_pexp : forall x u e,
    pexp e ->
    pexp u ->
    pexp ([x ~> u]e).
Proof.
  induction 1; simpl; intros; auto.
  - by calc_open_subst.
  - apply_fresh pexp_abs.
    + by apply subst_ptyp.
    + by rewrite~ <- subst_open_var_ptrm.
  - apply_fresh pexp_fix.
    + by auto using subst_ptyp.
    + replace (ptrm_abs ([x ~> u]T) ([x ~> u]e)) with ([x ~> u](ptrm_abs T e)) by reflexivity.
      rewrite~ <- subst_open_var_ptrm.
Qed.

Lemma subst_pnvalue : forall x u nv,
    pnvalue nv ->
    pexp u ->
    pnvalue ([x ~> u]nv).
Proof. induction 1; simpl; auto. Qed.

Lemma subst_pvalue : forall x u v,
    pvalue v ->
    pexp u ->
    pvalue ([x ~> u]v).
Proof.
  induction 1; simpl; intros; auto using subst_pnvalue.
  - apply pvalue_abs. by move: (subst_pexp x H H0) => /=.
Qed.

End PCFv.

Lemma subst_nvalue : forall x u nv,
    nvalue nv ->
    exp u ->
    nvalue ([x ~> u]nv).
Proof. induction 1; simpl; auto. Qed.

Lemma subst_typ_interf_exp_rbody_value :
  (forall T, typ T -> forall x u, exp u -> typ ([x ~> u]T)) /\
  (forall I, interf I -> forall x u, exp u -> interf ([x ~> u]I)) /\
  (forall e, exp e -> forall x u, exp u -> exp ([x ~> u]e)) /\
  (forall B, rbody B -> forall x u, exp u -> rbody ([x ~> u]B)) /\
  (forall v, value v -> forall x u, exp u -> value ([x ~> u]v)).
Proof.
  apply typ_interf_exp_rbody_value_ind; try easy; simpl; intros; f_equal; auto using subst_nvalue.
  - apply_fresh~ typ_refine. by rewrite~ <- subst_open_var_trm.
  - by calc_open_subst.
  - apply_fresh~ exp_abs. by rewrite~ <- subst_open_var_trm.
  - apply_fresh~ exp_fix; rewrite~ <- subst_open_var_trm.
Qed.

Lemma subst_typ : forall x u T, typ T -> exp u -> typ ([x ~> u]T).
Proof. intros. by apply subst_typ_interf_exp_rbody_value. Qed.

Lemma subst_interf : forall x u I, interf I -> exp u -> interf ([x ~> u]I).
Proof. intros. by apply subst_typ_interf_exp_rbody_value. Qed.

Lemma subst_exp : forall x u e, exp e -> exp u -> exp ([x ~> u]e).
Proof. intros. by apply subst_typ_interf_exp_rbody_value. Qed.

Lemma subst_rbody : forall x u B, rbody B -> exp u -> rbody ([x ~> u]B).
Proof. intros. by apply subst_typ_interf_exp_rbody_value. Qed.

Lemma subst_value : forall x u v, value v -> exp u -> value ([x ~> u]v).
Proof. intros. by apply subst_typ_interf_exp_rbody_value. Qed.

Hint Resolve subst_ptyp subst_pexp subst_pnvalue subst_pvalue
     subst_typ subst_interf subst_exp subst_rbody subst_value.

(** ** correspondence of essence function *)

Lemma essence_nvalue : forall nv,
    nvalue nv ->
    pnvalue (essence nv).
Proof. induction 1; simpl; auto. Qed.

Lemma essence_open_rec : forall k u t,
    essence ({k ~> u}t) = ({k ~> essence u}(essence t))%pcf.
Proof.
  intros. move: k u. induction t; simpl; intros; f_equal; auto.
  - by calc_open_subst.
Qed.

Lemma essence_open_var : forall x t,
    essence (t $^ x) = ((essence t) $^ x)%pcf.
Proof. intros. apply essence_open_rec. Qed.

Lemma essence_close_var_rec : forall k x t,
    essence (close_var_trm_rec k x t) = close_var_ptrm_rec k x (essence t).
Proof.
  intros. gen k. induction t; simpl; intros; f_equal; auto.
  - by calc_open_subst.
Qed.

Lemma essence_close_var : forall x t,
    essence (close_var_trm x t) = close_var_ptrm x (essence t).
Proof. intros. apply essence_close_var_rec. Qed.

Lemma close_var_ptyp : forall k x T,
    ptyp T ->
    close_var_ptrm_rec k x T = T.
Proof. induction 1; simpl; f_equal; auto. Qed.

Lemma close_var_open_ptrm_ind : forall x y z t1 i j,
  i <> j -> y <> x -> y \notin (fv_ptrm t1) ->
    ({i ~> ptrm_fvar y} ({j ~> ptrm_fvar z} (close_var_ptrm_rec j x t1))
     = {j ~> ptrm_fvar z} (close_var_ptrm_rec j x ({i ~> ptrm_fvar y}t1)))%pcf.
Proof.
  induction t1; simpl; intros; try solve [ fequals~ ].
  do 2 (case_nat; simpl); try solve [ case_var~ | case_nat~ ].
  case_var~. simpl. case_nat~.
Qed.

Lemma close_var_open_pexp : forall x y e,
    pexp e ->
    pexp ((close_var_ptrm x e) $^ y)%pcf.
Proof.
  intros. rewrite /close_var_ptrm /open_ptrm. generalize 0.
  induction H; simpl; intros; auto.
  - by calc_open_subst.
  - apply_fresh pexp_abs.
    + rewrite~ close_var_ptyp. rewrite~ <- open_ptyp.
    + calc_open_subst. rewrite~ close_var_open_ptrm_ind. by apply~ H1.
  - apply_fresh pexp_fix.
    + inverts H. do 2 rewrite~ close_var_ptyp. do 2 rewrite~ <- open_ptyp.
    + calc_open_subst. forwards~ K: (H1 y0). inverts K. apply_fresh pexp_abs.
      * rewrite~ close_var_open_ptrm_ind. forwards~ : (H0 y0). inverts H2.
        rewrite~ close_var_ptyp. rewrite~ <- open_ptyp.
      * forwards~ : (H5 y1). rewrite~ close_var_open_ptrm_ind.
Qed.

Lemma essence_typ_interf_exp_rbody_value :
  (forall T, typ T -> ptyp (essence T)) /\
  (forall I, interf I -> exists T1 T2, essence I = ptrm_arrow T1 T2 /\ ptyp (ptrm_arrow T1 T2)) /\
  (forall e, exp e -> pexp (essence e)) /\
  (forall B, rbody B -> exists T e, essence B = ptrm_abs T e /\ pexp (ptrm_abs T e)) /\
  (forall v, value v -> pvalue (essence v)).
Proof.
  apply typ_interf_exp_rbody_value_ind; simpl; intros; eauto using essence_nvalue.
  - apply_fresh~ pexp_abs. rewrite~ <- essence_open_var.
  - move: H0 => [T1 [T2 [-> K1]]].
    specialize_fresh H2. rewrite~ <- (@close_var_open_trm y B). rewrite essence_close_var.
    move: H0 => [T [e [-> K2]]]. rewrite /close_var_ptrm /=.
    apply_fresh~ pexp_fix. rewrite -[X in pexp (X $^ y0)]/(close_var_ptrm y (ptrm_abs T e)).
    apply~ close_var_open_pexp.
Qed.

Lemma essence_typ : forall T, typ T -> ptyp (essence T).
Proof. exact (proj51 essence_typ_interf_exp_rbody_value). Qed.

Lemma essence_exp : forall e, exp e -> pexp (essence e).
Proof. exact (proj53 essence_typ_interf_exp_rbody_value). Qed.

Lemma essence_value : forall v, value v -> pvalue (essence v).
Proof. exact (proj55 essence_typ_interf_exp_rbody_value). Qed.

Lemma essence_interf : forall I, interf I -> exists T1 T2, essence I = ptrm_arrow T1 T2.
Proof. intros. move: ((proj52 essence_typ_interf_exp_rbody_value) I H) => [T1 [T2 [Eq _]]]. eauto. Qed.

Lemma essence_rbody : forall B, rbody B -> exists T e, essence B = ptrm_abs T e.
Proof. intros. move: ((proj54 essence_typ_interf_exp_rbody_value) B H) => [T [e [E1 _]]]. eauto. Qed.

Hint Resolve essence_typ essence_exp essence_value essence_nvalue.

Lemma open_refine_body_exp : forall T e u,
    typ (trm_refine T e) ->
    exp u ->
    exp (e ^^ u).
Proof. intros. inverts H. specialize_fresh H4. rewrite* (@subst_intro_trm y). Qed.

Lemma open_abs_body_exp : forall T e u,
    exp (trm_abs T e) ->
    exp u ->
    exp (e ^^ u).
Proof. intros. inverts H. specialize_fresh H4. rewrite* (@subst_intro_trm y). Qed.

Lemma open_fix_body_rbody : forall I B u,
    exp (trm_fix I B) ->
    exp u ->
    rbody (B ^^ u).
Proof. intros. inverts H. specialize_fresh H4. rewrite~ (@subst_intro_trm y). Qed.

Lemma redpcf_regular_pre : forall e e',
    e -->pcf e' ->
    pexp e.
Proof. induction 1; auto. Qed.

Lemma redpcf_regular_post : forall e e',
    e -->pcf e' ->
    pexp e'.
Proof.
  induction 1; auto.
  - inverts H. pick_fresh y. rewrite~ (@subst_intro_ptrm y).
  - inverts keep H. specialize_fresh H5. rewrite~ (@subst_intro_ptrm y). simpl. auto.
Qed.

Lemma evalpcf_regular_pre : forall e e',
    e --->pcf e' ->
    pexp e.
Proof. induction 1; eauto using redpcf_regular_pre. Qed.

Lemma evalpcf_regular_post : forall e e',
    e --->pcf e' ->
    pexp e'.
Proof. induction 1; eauto using redpcf_regular_post. Qed.

Lemma mevalpcf_regular_pre : forall e e',
    mevalpcf e e' ->
    pexp e.
Proof. induction 1; eauto using evalpcf_regular_pre. Qed.

Lemma mevalpcf_regular_post : forall e e',
    mevalpcf e e' ->
    pexp e'.
Proof. induction 1; eauto using evalpcf_regular_post. Qed.

Lemma redp_regular_pre : forall e e',
    e -->p e' ->
    exp e.
Proof. induction 1; auto. Qed.

Lemma redp_regular_post : forall e e',
    e -->p e' ->
    exp e'.
Proof.
  induction 1; auto.
  - apply* open_abs_body_exp.
  - apply rbody_is_exp. apply* open_fix_body_rbody.
Qed.

Lemma redc_regular_pre : forall e e',
    e -->c e' ->
    exp e.
Proof. induction 1; auto. Qed.

Lemma redc_regular_post : forall e e',
    e -->c e' ->
    command e'.
Proof.
  induction 1; unfold command; try solve [ left;  auto].
  - left. apply* exp_active. apply* open_refine_body_exp.
  - right. by [].
Qed.

Lemma evalp_regular_pre : forall e e',
    e --->p e' ->
    exp e.
Proof. induction 1; eauto using redp_regular_pre. Qed.

Lemma evalp_regular_post : forall e e',
    e --->p e' ->
    exp e'.
Proof. induction 1; eauto using redp_regular_post. Qed.

Lemma evalc_regular_pre : forall e e',
    e --->c e' ->
    exp e.
Proof. induction 1; eauto using redc_regular_pre, evalp_regular_pre. Qed.

Lemma evalc_regular_post : forall e e',
    e --->c e' ->
    command e'.
Proof. induction 1; eauto using redc_regular_post, evalp_regular_post. Qed.

Lemma eval_regular_pre : forall e e',
    e ---> e' ->
    exp e.
Proof. induction 1; eauto using evalp_regular_pre, evalc_regular_pre. Qed.

Lemma eval_regular_post : forall e e',
    e ---> e' ->
    command e'.
Proof. induction 1; eauto using evalp_regular_post, evalc_regular_post. Qed.

Lemma meval_regular_pre : forall e e',
    e --->* e' ->
    exp e.
Proof. induction 1; eauto using eval_regular_pre. Qed.

Lemma meval_regular_post : forall e e',
    e --->* e' ->
    command e'.
Proof. induction 1; eauto using eval_regular_post. Qed.

Lemma exp_of_command : forall e,
    command e ->
    e <> trm_blame ->
    exp e.
Proof.
  intros. destruct H.
  - by [].
  - congruence.
Qed.

Hint Extern 1 (exp ?e) =>
match goal with
| [ H: e -->p ?e' |- _ ] => exact (redp_regular_pre H)
| [ H: ?e' -->p e |- _ ] => exact (redp_regular_post H)
| [ H: e -->c ?e' |- _ ] => exact (redc_regular_pre H)
| [ H: ?e' -->c e |- _ ] => refine (exp_of_command (redc_regular_post H) _); congruence
| [ H: e --->p ?e' |- _ ] => exact (evalp_regular_pre H)
| [ H: ?e' --->p e |- _ ] => exact (evalp_regular_post H)
| [ H: e --->c ?e' |- _ ] => exact (evalc_regular_pre H)
| [ H: ?e' --->c e |- _ ] => refine (exp_of_command (evalc_regular_post H) _); congruence
| [ H: e ---> ?e' |- _ ] => exact (eval_regular_pre H)
| [ H: ?e' ---> e |- _ ] => refine (exp_of_command (eval_regular_post H) _); congruence
| [ H: e --->* ?e' |- _ ] => exact (meval_regular_pre H)
| [ H: ?e' --->* e |- _ ] => refine (exp_of_command (meval_regular_post H) _); congruence
end.

Lemma environment_push_inv : forall E x T,
    environment (E & x ~ T) ->
    environment E /\ typ T.
Proof.
  inversion 1.
  - false* empty_push_inv.
  - apply eq_push_inv in H0. unpack H0. by subst.
Qed.

Lemma environment_push_inv_E : forall E x T,
    environment (E & x ~ T) ->
    environment E.
Proof. intros. by apply (environment_push_inv H). Qed.

Lemma environment_push_inv_T : forall E x T,
    environment (E & x ~ T) ->
    typ T.
Proof. intros. by apply (environment_push_inv H). Qed.

Lemma environment_single_inv : forall x T,
    environment (x ~ T) ->
    typ T.
Proof. intros. rewrite <- concat_empty_l in H. by apply (@environment_push_inv empty x T). Qed.

Lemma environment_binds_inv : forall x T E,
    environment E ->
    binds x T E ->
    typ T.
Proof.
  induction 1; intros.
  - false* binds_empty_inv.
  - binds_cases H2.
    + by auto.
    + by subst.
Qed.

Lemma distr_forall3 : forall P Q1 Q2 Q3,
    (forall x : var, P x -> Q1 x /\ Q2 x /\ Q3 x) ->
    (forall x : var, P x -> Q1 x) /\
    (forall x : var, P x -> Q2 x) /\
    (forall x : var, P x -> Q3 x).
Proof. intros. splits 3; intros x PH; apply (H x PH). Qed.

Local Ltac distr_forall3 H :=
  let K1 := fresh "K" in
  let K2 := fresh "K" in
  let K3 := fresh "K" in
  apply distr_forall3 in H as (K1 & K2 & K3).

Lemma wf_env_wf_typ_typing_regular :
  (forall E, wf_env E -> environment E) /\
  (forall T, wf_typ T -> typ T) /\
  (forall E e T, E |= e ~: T -> environment E /\ exp e /\ typ T).
Proof.
  apply wf_env_wf_typ_typing_ind; intros; try splits 3; autos*.
  - distr_forall3 H0. apply_fresh~ typ_refine.
    + specialize_fresh K. by apply (environment_single_inv H0).
  - exact (environment_binds_inv H0 H1).
  - specialize_fresh H0. intuition eauto using environment_push_inv_E.
  - distr_forall3 H0. apply_fresh~ exp_abs.
    + specialize_fresh K. by apply (environment_push_inv H0).
  - specialize_fresh H0. intuition eauto using environment_push_inv_T.
  - specialize_fresh H2. intuition eauto using environment_push_inv_E.
Qed.

Lemma wf_env_regular : forall E,
    wf_env E -> environment E.
Proof. by apply wf_env_wf_typ_typing_regular. Qed.

Lemma wf_typ_regular : forall T,
    wf_typ T -> typ T.
Proof. by apply wf_env_wf_typ_typing_regular. Qed.

Lemma typing_regular_E : forall E e T,
    E |= e ~: T -> environment E.
Proof. intros. by apply ((proj33 wf_env_wf_typ_typing_regular) E e T). Qed.

Lemma typing_regular_e : forall E e T,
    E |= e ~: T -> exp e.
Proof. intros. by apply ((proj33 wf_env_wf_typ_typing_regular) E e T). Qed.

Lemma typing_regular_T : forall E e T,
    E |= e ~: T -> typ T.
Proof. intros. by apply ((proj33 wf_env_wf_typ_typing_regular) E e T). Qed.

Hint Resolve wf_env_regular wf_typ_regular.

Hint Extern 1 (environment ?E) =>
match goal with
| [ H: E |= _ ~: _ |- _ ] => exact (typing_regular_E H)
end.

Hint Extern 1 (exp ?e) =>
match goal with
| [ H: _ |= e ~: _ |- _ ] => exact (typing_regular_e H)
end.

Hint Extern 1 (typ ?T) =>
match goal with
| [ H: _ |= _ ~: T |- _ ] => exact (typing_regular_T H)
end.
