Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From DH Require Export  DH_Definition.

(** ** tactics dealing variables *)

Ltac gather_vars :=
  let A := gather_vars_with (fun x : var => \{x}) in
  let B := gather_vars_with (fun x : vars => x) in
  let C := gather_vars_with (fun x : ptrm => fv_ptrm x) in
  let D := gather_vars_with (fun x : trm => fv_trm x) in
  let E := gather_vars_with (fun x : env => dom x) in
  constr:(A \u B \u C \u D \u E).

Tactic Notation "pick_fresh" ident(x) :=
  let L := gather_vars in (pick_fresh_gen L x).

Tactic Notation "pick_fresh" ident(x) "from" constr(E) :=
  let L := gather_vars in (pick_fresh_gen (L \u E) x).

Tactic Notation "apply_fresh" constr(H) :=
  apply_fresh_base H gather_vars ltac_no_arg.

Tactic Notation "apply_fresh" "~" constr(H) :=
  apply_fresh H; auto_tilde.

Tactic Notation "apply_fresh" "*" constr(H) :=
  apply_fresh H; auto_star.

Ltac calc_open_subst :=
  unfold open_ptrm, open_trm; repeat (simpl; first [case_nat | case_var]); simpl.

Tactic Notation "calc_open_subst" "in" constr(H) :=
  unfold open_ptrm, open_trm in H; repeat (simpl in H; first [case_nat | case_var]); simpl in H.

Ltac specialize_fresh H :=
  match type of H with
  | forall x, x \notin _ -> _ =>
    let y := fresh "y" in pick_fresh y; forwards : (H y); first by notin_solve; clear H
  end.

(** ** induction principles for mutually inductive definitions *)

Scheme Minimality for typ Sort Prop
  with Minimality for interf Sort Prop
  with Minimality for exp Sort Prop
  with Minimality for rbody Sort Prop
  with Minimality for value Sort Prop.
Combined Scheme typ_interf_exp_rbody_value_ind from typ_ind, interf_ind, exp_ind, rbody_ind, value_ind.

Scheme Minimality for wf_env Sort Prop
  with Minimality for wf_typ Sort Prop
  with Minimality for typing Sort Prop.
Combined Scheme wf_env_wf_typ_typing_ind from wf_env_ind, wf_typ_ind, typing_ind.

(** * common properties for locally-closed terms *)

Section PCFv.
Open Scope pcf_scope.

Lemma open_ptrm_rec : forall j v t i u,
    {j ~> v}t = {i ~> u}({j ~> v}t) ->
    i <> j ->
    t = {i ~> u}t.
Proof.
  move=> j v t. move: j v. induction t; simpl; introv Eq Neq; inverts Eq; f_equal*.
  - case_nat~. case_nat~.
Qed.

Lemma open_ptyp : forall k u T,
    ptyp T ->
    T = ({k ~> u}T).
Proof. induction 1; simpl; f_equal; auto. Qed.

Lemma open_pexp : forall k u e,
    pexp e ->
    e = {k ~> u}e.
Proof.
  intros. move: k. induction H; simpl; intros; f_equal; auto using open_ptyp.
  - specialize_fresh H1. by eauto using open_ptrm_rec.
  - replace (ptrm_arrow ({k ~> u}T1) ({k ~> u}T2)) with ({k ~> u}(ptrm_arrow T1 T2)) by reflexivity.
    by apply open_ptyp.
  - replace (ptrm_abs ({S k ~> u}T) ({S (S k) ~> u}e)) with ({S k ~> u}(ptrm_abs T e)) by reflexivity.
    specialize_fresh H1. by eauto using open_ptrm_rec.
Qed.

Lemma subst_open_ptrm_rec : forall x u v t k,
    pexp u ->
    [x ~> u]({k ~> v}t) = {k ~> [x ~> u]v}([x ~> u]t).
Proof.
  induction t; simpl; intros; f_equal; auto.
  - by calc_open_subst.
  - case_var~. by apply open_pexp.
Qed.

Lemma subst_open_ptrm : forall x u v t,
    pexp u ->
    [x ~> u](t ^^ v) = ([x ~> u]t) ^^ ([x ~> u]v).
Proof. calc_open_subst. intros. by apply subst_open_ptrm_rec. Qed.

Lemma subst_open_var_ptrm : forall x y u t,
    pexp u ->
    x <> y ->
    [x ~> u](t $^ y) = ([x ~> u]t) $^ y.
Proof. intros. rewrite~ subst_open_ptrm. by calc_open_subst. Qed.

Lemma subst_fresh_ptrm : forall x u t,
    x \notin (fv_ptrm t) ->
    [x ~> u]t = t.
Proof.
  induction t; simpl; intros; f_equal; auto.
  - by calc_open_subst.
Qed.

Lemma subst_intro_ptrm : forall x u t,
    x \notin (fv_ptrm t) ->
    pexp u ->
    t ^^ u = [x ~> u](t $^ x).
Proof. intros. rewrite~ subst_open_ptrm. calc_open_subst. by rewrite subst_fresh_ptrm. Qed.

End PCFv.

Lemma open_term_rec : forall j v t i u,
    {j ~> v}t = {i ~> u}({j ~> v}t) ->
    i <> j ->
    t = {i ~> u}t.
Proof.
  move=> j v t. move: j v.
  induction t; simpl; introv Eq Neq; inverts Eq; f_equal*.
  - case_nat~. case_nat~.
Qed.

Lemma open_nvalue : forall k u nv,
    nvalue nv ->
    nv = {k ~> u}nv.
Proof. induction 1; simpl; congruence. Qed.

Lemma open_typ_interf_exp_rbody_value :
  (forall T, typ T -> forall k u, T = {k ~> u}T) /\
  (forall I, interf I -> forall k u, I = {k ~> u}I) /\
  (forall e, exp e -> forall k u, e = {k ~> u}e) /\
  (forall B, rbody B -> forall k u, B = {k ~> u}B) /\
  (forall v, value v -> forall k u, v = {k ~> u}v).
Proof.
  apply typ_interf_exp_rbody_value_ind; try easy; simpl; intros; f_equal; auto using open_nvalue.
  - specialize_fresh H2. by eauto using open_term_rec.
  - specialize_fresh H2. by eauto using open_term_rec.
  - specialize_fresh H2. by eauto using open_term_rec.
Qed.

Lemma open_typ : forall k u T, typ T -> T = {k ~> u}T.
Proof. auto using (proj51 open_typ_interf_exp_rbody_value). Qed.

Lemma open_exp : forall k u e, exp e -> e = {k ~> u}e.
Proof. auto using (proj53 open_typ_interf_exp_rbody_value). Qed.

Lemma subst_open_trm_rec : forall x u v t k,
    exp u ->
    [x ~> u]({k ~> v}t) = {k ~> [x ~> u]v}([x ~> u]t).
Proof.
  induction t; simpl; intros; f_equal; auto.
  - case_nat~.
  - case_var~. by apply open_exp.
Qed.

Lemma subst_open_trm : forall x u v t,
    exp u ->
    [x ~> u](t ^^ v) = ([x ~> u]t) ^^ ([x ~> u]v).
Proof. calc_open_subst. intros. by apply subst_open_trm_rec. Qed.

Lemma subst_open_var_trm : forall x y u t,
    exp u ->
    x <> y ->
    [x ~> u](t $^ y) = ([x ~> u]t) $^ y.
Proof. intros. rewrite~ subst_open_trm. by calc_open_subst. Qed.

Lemma subst_fresh_trm : forall x u t,
    x \notin (fv_trm t) ->
    [x ~> u]t = t.
Proof.
  induction t; simpl; intros; f_equal; auto.
  - by calc_open_subst.
Qed.

Lemma subst_intro_trm : forall x u t,
    x \notin (fv_trm t) ->
    exp u ->
    t ^^ u = [x ~> u](t $^ x).
Proof. intros. rewrite~ subst_open_trm. calc_open_subst. by rewrite subst_fresh_trm. Qed.


Lemma close_var_open_trm_rec : forall k x t,
    x \notin fv_trm t ->
    close_var_trm_rec k x ({k ~> trm_fvar x}t) = t.
Proof.
  introv. gen k. induction t; simpl; intros; f_equal; auto.
  - by calc_open_subst.
  - by calc_open_subst.
Qed.

Lemma close_var_open_trm : forall x t,
    x \notin fv_trm t ->
    close_var_trm x (t $^ x) = t.
Proof. intros. by apply close_var_open_trm_rec. Qed.

(** ** automation *)

Hint Constructors ptyp pexp pnvalue pvalue
     nvalue typ interf exp rbody value environment
     redpcf evalpcf redp redc evalp evalc eval wf_env wf_typ typing.
Remove Hints pexp_abs pexp_fix typ_refine exp_abs exp_fix wf_typ_refine typing_abs typing_fix.

Hint Extern 2 (pexp (trm_abs _ _)) => apply_fresh pexp_abs.
Hint Extern 2 (pexp (trm_fix _ _)) => apply_fresh pexp_fix.
Hint Extern 2 (typ (trm_refine _ _)) => apply_fresh typ_refine.
Hint Extern 2 (exp (trm_abs _ _)) => apply_fresh exp_abs.
Hint Extern 2 (exp (trm_fix _ _)) => apply_fresh exp_fix.
Hint Extern 1 (wf_typ (trm_refine _ _)) => apply_fresh wf_typ_refine.
Hint Extern 1 (_ |= (trm_abs _ _) ~: _) => apply_fresh typing_abs.
Hint Extern 2 (_ |= (trm_fix _ _) ~: _) => apply_fresh typing_fix.

(** attemption obtaining locally-closedness from sub-terms *)

Hint Extern 10 (ptyp ?T) =>
match goal with
| [ H: ptyp ?t |- _ ] =>
  match t with T => first [ exact H | fail 1 ] | context [T] => inversion H end
| [ H: pexp ?t |- _ ] =>
  match t with T => fail 1 | context [T] => inversion H end
| [ H: pvalue ?t |- _ ] =>
  match t with T => fail 1 | context [T] => inversion H end
end.

Hint Extern 10 (pexp ?e) =>
match goal with
| [ H: pexp ?t |- _ ] =>
  match t with e => first [ exact H | fail 1 ] | context [e] => inversion H end
end.

Hint Extern 10 (pnvalue ?nv) =>
match goal with
| [ H: pnvalue ?t |- _ ] =>
  match t with nv => first [ exact H | fail 1 ] | context [nv] => inversion H end
| [ H: pvalue ?t |- _ ] =>
  match t with nv => fail 1 | context [nv] => inversion H end
end.

Hint Extern 10 (typ ?T) =>
match goal with
| [ H: typ ?t |- _ ] =>
  match t with T => first [ exact H | fail 1 ] | context [T] => inversion H end
| [ H: exp ?t |- _ ] =>
  match t with T => fail 1 | context [T] => inversion H end
end.

Hint Extern 10 (exp ?e) =>
match goal with
| [ H: exp ?t |- _ ] =>
  match t with e => first [ exact H | fail 1 ] | context [e] => inversion H end
end.

Hint Extern 10 (value ?v) =>
match goal with
| [ H: exp ?t |- _ ] =>
  match t with v => first [ exact H | fail 1 ] | context [v] => inversion H end
end.
