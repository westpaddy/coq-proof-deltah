# Mechanized formalization of PCFvD in Coq

## Requirement (versions are which we used during development)
- Coq 8.9.1
- coq-tlc 20181116 (can be installed via opam)

## Files
- `DH_Definition.v`
	- All definitions for the system (e.g., terms, semantics, typing
      system, etc.)
- `DH_Infrastructure.v`
	- Miscellaneous properties used for locally-nameless technique
	- Hints and Tactics for basic automation
- `DH_Regularity.v`
	- Properties showing well-definedness of the definition
- `DH_Soundness.v`
	- Main development of PCFvD properties.  Correspondence between
      the theorems in the paper and the lemma names in the file is as
      follow.
		- Theorem 1: `non_destructive_testing`
		- Theorem 2: `preservation`
		- Theorem 3: `value_inversion`
		- Theorem 4: `progress_result`
