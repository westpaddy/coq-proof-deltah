Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
From TLC Require Export LibLN.

(** * Raw terms *)

(** ** PCFv raw terms  *)
(** [ptrm_bottom] is used for defining [essence], which is a partial
    function from PCFvD raw terms to PCFv terms. *)
Inductive ptrm : Set :=
| ptrm_bvar : nat -> ptrm
| ptrm_nat : ptrm
| ptrm_bool : ptrm
| ptrm_arrow : ptrm -> ptrm -> ptrm
| ptrm_zero : ptrm
| ptrm_succ : ptrm -> ptrm
| ptrm_pred : ptrm -> ptrm
| ptrm_iszero : ptrm -> ptrm
| ptrm_true : ptrm
| ptrm_false : ptrm
| ptrm_if : ptrm -> ptrm -> ptrm -> ptrm
| ptrm_fvar : var -> ptrm
| ptrm_app : ptrm -> ptrm -> ptrm
| ptrm_abs : ptrm -> ptrm -> ptrm
| ptrm_fix : ptrm -> ptrm -> ptrm
| ptrm_bottom : ptrm.

Bind Scope pcf_scope with ptrm.
Delimit Scope pcf_scope with pcf.

(** ** Operations for PCFv raw terms *)

(** *** Opening operation from locally-nameless representation *)
(** We assume [[u]] is locally-closed. *)
Reserved Notation "{ k ~> u } t" (at level 67, t at level 9).
Fixpoint open_ptrm_rec (k : nat) (u : ptrm) (t : ptrm) {struct t} : ptrm :=
  match t with
  | ptrm_bvar i => If i = k then u else ptrm_bvar i
  | ptrm_nat => ptrm_nat
  | ptrm_bool => ptrm_bool
  | ptrm_arrow t1 t2 => ptrm_arrow ({k ~> u}t1) ({k ~> u}t2)
  | ptrm_zero => ptrm_zero
  | ptrm_succ t1 => ptrm_succ ({k ~> u}t1)
  | ptrm_pred t1 => ptrm_pred ({k ~> u}t1)
  | ptrm_iszero t1 => ptrm_iszero ({k ~> u}t1)
  | ptrm_true => ptrm_true
  | ptrm_false => ptrm_false
  | ptrm_if t1 t2 t3 => ptrm_if ({k ~> u}t1) ({k ~> u}t2) ({k ~> u}t3)
  | ptrm_fvar x => ptrm_fvar x
  | ptrm_app t1 t2 => ptrm_app ({k ~> u}t1) ({k ~> u}t2)
  | ptrm_abs t1 t2 => ptrm_abs ({k ~> u}t1) ({(S k) ~> u}t2)
  | ptrm_fix t1 t2 => ptrm_fix ({k ~> u}t1) ({(S k) ~> u}t2)
  | ptrm_bottom => ptrm_bottom
  end
where "{ k ~> u } t" := (open_ptrm_rec k u t) : pcf_scope.

Definition open_ptrm t u := ({0 ~> u}t)%pcf.
Notation "t ^^ u" := (open_ptrm t u) (at level 67) : pcf_scope.
Notation "t $^ x" := (open_ptrm t (ptrm_fvar x)) (at level 67, x global) : pcf_scope.

(** *** Closing operation from locally-nameless representation *)
Fixpoint close_var_ptrm_rec (k : nat) (z : var) (t : ptrm) {struct t} : ptrm :=
  match t with
  | ptrm_bvar i => ptrm_bvar i
  | ptrm_nat => ptrm_nat
  | ptrm_bool => ptrm_bool
  | ptrm_arrow t1 t2 => ptrm_arrow (close_var_ptrm_rec k z t1) (close_var_ptrm_rec k z t2)
  | ptrm_zero => ptrm_zero
  | ptrm_succ t1 => ptrm_succ (close_var_ptrm_rec k z t1)
  | ptrm_pred t1 => ptrm_pred (close_var_ptrm_rec k z t1)
  | ptrm_iszero t1 => ptrm_iszero (close_var_ptrm_rec k z t1)
  | ptrm_true => ptrm_true
  | ptrm_false => ptrm_false
  | ptrm_if t1 t2 t3 => ptrm_if (close_var_ptrm_rec k z t1) (close_var_ptrm_rec k z t2) (close_var_ptrm_rec k z t3)
  | ptrm_fvar x => If x = z then ptrm_bvar k else ptrm_fvar x
  | ptrm_app t1 t2 => ptrm_app (close_var_ptrm_rec k z t1) (close_var_ptrm_rec k z t2)
  | ptrm_abs t1 t2 => ptrm_abs (close_var_ptrm_rec k z t1) (close_var_ptrm_rec (S k) z t2)
  | ptrm_fix t1 t2 => ptrm_fix (close_var_ptrm_rec k z t1) (close_var_ptrm_rec (S k) z t2)
  | ptrm_bottom => ptrm_bottom
  end.

Definition close_var_ptrm x t := (close_var_ptrm_rec 0 x t).

(** collecting all free variables in a PCFv term *)
Fixpoint fv_ptrm (t : ptrm) {struct t} : vars :=
  match t with
  | ptrm_bvar i => \{}
  | ptrm_nat =>  \{}
  | ptrm_bool => \{}
  | ptrm_arrow t1 t2 => (fv_ptrm t1) \u (fv_ptrm t2)
  | ptrm_zero => \{}
  | ptrm_succ t1 => fv_ptrm t1
  | ptrm_pred t1 => fv_ptrm t1
  | ptrm_iszero t1 => fv_ptrm t1
  | ptrm_true => \{}
  | ptrm_false => \{}
  | ptrm_if t1 t2 t3 => (fv_ptrm t1) \u (fv_ptrm t2) \u (fv_ptrm t3)
  | ptrm_fvar x => \{x}
  | ptrm_app t1 t2 => (fv_ptrm t1) \u (fv_ptrm t2)
  | ptrm_abs t1 t2 => (fv_ptrm t1) \u (fv_ptrm t2)
  | ptrm_fix t1 t2 => (fv_ptrm t1) \u (fv_ptrm t2)
  | ptrm_bottom => \{}
  end.

(** replacing all [z] in [t] with [u] *)
(** Note we assume [u] is locall-closed, and thus there are no need to
    capture-avoiding *)
Reserved Notation "[ x ~> u ]  t" (at level 66, t at level 9).
Fixpoint subst_ptrm (z : var) (u : ptrm) (t : ptrm) {struct t} : ptrm :=
  match t with
  | ptrm_bvar i => ptrm_bvar i
  | ptrm_nat => ptrm_nat
  | ptrm_bool => ptrm_bool
  | ptrm_arrow t1 t2 => ptrm_arrow ([z ~> u]t1) ([z ~> u]t2)
  | ptrm_zero => ptrm_zero
  | ptrm_succ t1 => ptrm_succ ([z ~> u]t1)
  | ptrm_pred t1 => ptrm_pred ([z ~> u]t1)
  | ptrm_iszero t1 => ptrm_iszero ([z ~> u]t1)
  | ptrm_true => ptrm_true
  | ptrm_false => ptrm_false
  | ptrm_if t1 t2 t3 => ptrm_if ([z ~> u]t1) ([z ~> u]t2) ([z ~> u]t3)
  | ptrm_fvar x => If x = z then u else ptrm_fvar x
  | ptrm_app t1 t2 => ptrm_app ([z ~> u]t1) ([z ~> u]t2)
  | ptrm_abs t1 t2 => ptrm_abs ([z ~> u]t1) ([z ~> u]t2)
  | ptrm_fix t1 t2 => ptrm_fix ([z ~> u]t1) ([z ~> u]t2)
  | ptrm_bottom => ptrm_bottom
  end
where "[ z ~> u ] t" := (subst_ptrm z u t) : pcf_scope.

(** ** PCFvD raw terms *)

Inductive trm : Set :=
| trm_bvar : nat -> trm
| trm_nat : trm
| trm_bool : trm
| trm_arrow : trm -> trm -> trm
| trm_wedge : trm -> trm -> trm
| trm_refine : trm -> trm -> trm
| trm_zero : trm
| trm_succ : trm -> trm
| trm_pred : trm -> trm
| trm_iszero : trm -> trm
| trm_true : trm
| trm_false : trm
| trm_if : trm -> trm -> trm -> trm
| trm_fvar : var -> trm
| trm_app : trm -> trm -> trm
| trm_abs : trm -> trm -> trm
| trm_pair : trm -> trm -> trm
| trm_fst : trm -> trm
| trm_snd : trm -> trm
| trm_fix : trm -> trm -> trm
| trm_cast : trm -> trm -> trm -> trm
| trm_delay : trm -> trm -> trm -> trm
| trm_waiting : trm -> trm -> trm
| trm_active : trm -> trm -> trm -> trm
| trm_blame : trm.

Definition env := LibEnv.env trm.

(** ** operations for PCFvD raw terms *)

Reserved Notation "{ k ~> u } t" (at level 67, t at level 9).
Fixpoint open_trm_rec (k : nat) (u : trm) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i => If i = k then u else trm_bvar i
  | trm_nat => trm_nat
  | trm_bool => trm_bool
  | trm_arrow t1 t2 => trm_arrow ({k ~> u}t1) ({k ~> u}t2)
  | trm_wedge t1 t2 => trm_wedge ({k ~> u}t1) ({k ~> u}t2)
  | trm_refine t1 t2 => trm_refine ({k ~> u}t1) ({S k ~> u}t2)
  | trm_zero => trm_zero
  | trm_succ t1 => trm_succ ({k ~> u}t1)
  | trm_pred t1 => trm_pred ({k ~> u}t1)
  | trm_iszero t1 => trm_iszero ({k ~> u}t1)
  | trm_true => trm_true
  | trm_false => trm_false
  | trm_if t1 t2 t3 => trm_if ({k ~> u}t1) ({k ~> u}t2) ({k ~> u}t3)
  | trm_fvar x => trm_fvar x
  | trm_app t1 t2 => trm_app ({k ~> u}t1) ({k ~> u}t2)
  | trm_abs t1 t2 => trm_abs ({k ~> u}t1) ({S k ~> u}t2)
  | trm_pair t1 t2 => trm_pair ({k ~> u}t1) ({k ~> u}t2)
  | trm_fst t1 => trm_fst ({k ~> u}t1)
  | trm_snd t1 => trm_snd ({k ~> u}t1)
  | trm_fix t1 t2 => trm_fix ({k ~> u}t1) ({S k ~> u}t2)
  | trm_cast t1 t2 t3 => trm_cast ({k ~> u}t1) ({k ~> u}t2) ({k ~> u}t3)
  | trm_delay t1 t2 t3 => trm_delay ({k ~> u}t1) ({k ~> u}t2) ({k ~> u}t3)
  | trm_waiting t1 t2 => trm_waiting ({k ~> u}t1) ({k ~> u}t2)
  | trm_active t1 t2 t3 => trm_active ({k ~> u}t1) ({k ~> u}t2) ({k ~> u}t3)
  | trm_blame => trm_blame
  end
where "{ k ~> u } t" := (open_trm_rec k u t).

Definition open_trm t u := {0 ~> u}t.
Notation "t ^^ u" := (open_trm t u) (at level 67).
Notation "t $^ x" := (open_trm t (trm_fvar x)) (at level 67, x global).

Fixpoint close_var_trm_rec (k : nat) (z : var) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i => trm_bvar i
  | trm_nat => trm_nat
  | trm_bool => trm_bool
  | trm_arrow t1 t2 => trm_arrow (close_var_trm_rec k z t1) (close_var_trm_rec k z t2)
  | trm_wedge t1 t2 => trm_wedge (close_var_trm_rec k z t1) (close_var_trm_rec k z t2)
  | trm_refine t1 t2 => trm_refine (close_var_trm_rec k z t1) (close_var_trm_rec (S k) z t2)
  | trm_zero => trm_zero
  | trm_succ t1 => trm_succ (close_var_trm_rec k z t1)
  | trm_pred t1 => trm_pred (close_var_trm_rec k z t1)
  | trm_iszero t1 => trm_iszero (close_var_trm_rec k z t1)
  | trm_true => trm_true
  | trm_false => trm_false
  | trm_if t1 t2 t3 => trm_if (close_var_trm_rec k z t1) (close_var_trm_rec k z t2) (close_var_trm_rec k z t3)
  | trm_fvar x => If x = z then trm_bvar k else trm_fvar x
  | trm_app t1 t2 => trm_app (close_var_trm_rec k z t1) (close_var_trm_rec k z t2)
  | trm_abs t1 t2 => trm_abs (close_var_trm_rec k z t1) (close_var_trm_rec (S k) z t2)
  | trm_pair t1 t2 => trm_pair (close_var_trm_rec k z t1) (close_var_trm_rec k z t2)
  | trm_fst t1 => trm_fst (close_var_trm_rec k z t1)
  | trm_snd t1 => trm_snd (close_var_trm_rec k z t1)
  | trm_fix t1 t2 => trm_fix (close_var_trm_rec k z t1) (close_var_trm_rec (S k) z t2)
  | trm_cast t1 t2 t3 => trm_cast (close_var_trm_rec k z t1) (close_var_trm_rec k z t2) (close_var_trm_rec k z t3)
  | trm_delay t1 t2 t3 => trm_delay (close_var_trm_rec k z t1) (close_var_trm_rec k z t2) (close_var_trm_rec k z t3)
  | trm_waiting t1 t2 => trm_waiting (close_var_trm_rec k z t1) (close_var_trm_rec k z t2)
  | trm_active t1 t2 t3 => trm_active (close_var_trm_rec k z t1) (close_var_trm_rec k z t2) (close_var_trm_rec k z t3)
  | trm_blame => trm_blame
  end.

Definition close_var_trm x t := close_var_trm_rec 0 x t.

(** collecting all free variables in a pcfvd term *)
Fixpoint fv_trm (t : trm) {struct t} : vars :=
  match t with
  | trm_bvar i => \{}
  | trm_nat => \{}
  | trm_bool => \{}
  | trm_arrow t1 t2 => (fv_trm t1) \u (fv_trm t2)
  | trm_wedge t1 t2 => (fv_trm t1) \u (fv_trm t2)
  | trm_refine t1 t2 => (fv_trm t1) \u (fv_trm t2)
  | trm_zero => \{}
  | trm_succ t1 => fv_trm t1
  | trm_pred t1 => fv_trm t1
  | trm_iszero t1 => fv_trm t1
  | trm_true => \{}
  | trm_false => \{}
  | trm_if t1 t2 t3 => (fv_trm t1) \u (fv_trm t2) \u (fv_trm t3)
  | trm_fvar x => \{x}
  | trm_app t1 t2 => (fv_trm t1) \u (fv_trm t2)
  | trm_abs t1 t2 => (fv_trm t1) \u (fv_trm t2)
  | trm_pair t1 t2 => (fv_trm t1) \u (fv_trm t2)
  | trm_fst t1 => fv_trm t1
  | trm_snd t1 => fv_trm t1
  | trm_fix t1 t2 => (fv_trm t1) \u (fv_trm t2)
  | trm_cast t1 t2 t3 => (fv_trm t1) \u (fv_trm t2) \u (fv_trm t3)
  | trm_delay t1 t2 t3 => (fv_trm t1) \u (fv_trm t2) \u (fv_trm t3)
  | trm_waiting t1 t2 => (fv_trm t1) \u (fv_trm t2)
  | trm_active t1 t2 t3 => (fv_trm t1) \u (fv_trm t2) \u (fv_trm t3)
  | trm_blame => \{}
  end.

(** replacing all [z] in [t] with [u] *)
(** Note we assume [u] is locall-closed, and thus there are no need to capture-avoiding *)
Fixpoint subst_trm (z : var) (u : trm) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i => trm_bvar i
  | trm_nat => trm_nat
  | trm_bool => trm_bool
  | trm_arrow t1 t2 => trm_arrow ([z ~> u]t1) ([z ~> u]t2)
  | trm_wedge t1 t2 => trm_wedge ([z ~> u]t1) ([z ~> u]t2)
  | trm_refine t1 t2 => trm_refine ([z ~> u]t1) ([z ~> u]t2)
  | trm_zero => trm_zero
  | trm_succ t1 => trm_succ ([z ~> u]t1)
  | trm_pred t1 => trm_pred ([z ~> u]t1)
  | trm_iszero t1 => trm_iszero ([z ~> u]t1)
  | trm_true => trm_true
  | trm_false => trm_false
  | trm_if t1 t2 t3 => trm_if ([z ~> u]t1) ([z ~> u]t2) ([z ~> u]t3)
  | trm_fvar x => If x = z then u else trm_fvar x
  | trm_app t1 t2 => trm_app ([z ~> u]t1) ([z ~> u]t2)
  | trm_abs t1 t2 => trm_abs ([z ~> u]t1) ([z ~> u]t2)
  | trm_pair t1 t2 => trm_pair ([z ~> u]t1) ([z ~> u]t2)
  | trm_fst t1 => trm_fst ([z ~> u]t1)
  | trm_snd t1 => trm_snd ([z ~> u]t1)
  | trm_fix t1 t2 => trm_fix ([z ~> u]t1) ([z ~> u]t2)
  | trm_cast t1 t2 t3 => trm_cast ([z ~> u]t1) ([z ~> u]t2) ([z ~> u]t3)
  | trm_delay t1 t2 t3 => trm_delay ([z ~> u]t1) ([z ~> u]t2) ([z ~> u]t3)
  | trm_waiting t1 t2 => trm_waiting ([z ~> u]t1) ([z ~> u]t2)
  | trm_active t1 t2 t3 => trm_active ([z ~> u]t1) ([z ~> u]t2) ([z ~> u]t3)
  | trm_blame => trm_blame
  end
where "[ z ~> u ] t" := (subst_trm z u t).

(** conversion from MCS terms into PCFv terms *)
Fixpoint essence (t : trm) {struct t} : ptrm :=
  match t with
  | trm_bvar i => ptrm_bvar i
  | trm_nat => ptrm_nat
  | trm_bool => ptrm_bool
  | trm_arrow t1 t2 => ptrm_arrow (essence t1) (essence t2)
  | trm_wedge t1 t2 => essence t1
  | trm_refine t1 t2 => essence t1
  | trm_zero => ptrm_zero
  | trm_succ t1 => ptrm_succ (essence t1)
  | trm_pred t1 => ptrm_pred (essence t1)
  | trm_iszero t1 => ptrm_iszero (essence t1)
  | trm_true => ptrm_true
  | trm_false => ptrm_false
  | trm_if t1 t2 t3 => ptrm_if (essence t1) (essence t2) (essence t3)
  | trm_fvar x => ptrm_fvar x
  | trm_app t1 t2 => ptrm_app (essence t1) (essence t2)
  | trm_abs t1 t2 => ptrm_abs (essence t1) (essence t2)
  | trm_pair t1 t2 => essence t1
  | trm_fst t1 => essence t1
  | trm_snd t1 => essence t1
  | trm_fix t1 t2 => ptrm_fix (essence t1) (essence t2)
  | trm_cast t1 t2 t3 => essence t1
  | trm_delay t1 t2 t3 => essence t1
  | trm_waiting t1 t2 => essence t1
  | trm_active t1 t2 t3 => essence t2
  | trm_blame => ptrm_bottom
  end.

(** * Syntactic categories *)

(** ** PCFv syntax *)

(** PCFv types *)
Inductive ptyp : ptrm -> Prop :=
| ptyp_nat :
    ptyp ptrm_nat
| ptyp_bool :
    ptyp ptrm_bool
| ptyp_arrow : forall T1 T2,
    ptyp T1 ->
    ptyp T2 ->
    ptyp (ptrm_arrow T1 T2).

(** PCFv expressions *)
Inductive pexp : ptrm -> Prop :=
| pexp_zero :
    pexp ptrm_zero
| pexp_succ : forall e,
    pexp e ->
    pexp (ptrm_succ e)
| pexp_pred : forall e,
    pexp e ->
    pexp (ptrm_pred e)
| pexp_iszero : forall e,
    pexp e ->
    pexp (ptrm_iszero e)
| pexp_true :
    pexp ptrm_true
| pexp_false :
    pexp ptrm_false
| pexp_if : forall e1 e2 e3,
    pexp e1 ->
    pexp e2 ->
    pexp e3 ->
    pexp (ptrm_if e1 e2 e3)
| pexp_var : forall x,
    pexp (ptrm_fvar x)
| pexp_app : forall e1 e2,
    pexp e1 ->
    pexp e2 ->
    pexp (ptrm_app e1 e2)
| pexp_abs : forall L T e,
    ptyp T ->
    (forall x, x \notin L -> pexp (e $^ x)%pcf) ->
    pexp (ptrm_abs T e)
| pexp_fix : forall L T T1 T2 e,
    ptyp (ptrm_arrow T1 T2) ->
    (forall f, f \notin L -> pexp ((ptrm_abs T e) $^ f)%pcf) ->
    pexp (ptrm_fix (ptrm_arrow T1 T2) (ptrm_abs T e)).

(** PCFv numerals *)
Inductive pnvalue : ptrm -> Prop :=
| pnvalue_zero :
    pnvalue ptrm_zero
| pnvalue_succ : forall nv,
    pnvalue nv ->
    pnvalue (ptrm_succ nv).

(** PCFv values *)
Inductive pvalue : ptrm -> Prop :=
| pvalue_zero :
    pvalue ptrm_zero
| pvalue_succ : forall nv,
    pnvalue nv ->
    pvalue (ptrm_succ nv)
| pvalue_true :
    pvalue ptrm_true
| pvalue_false :
    pvalue ptrm_false
| pvalue_abs : forall T e,
    pexp (ptrm_abs T e) ->
    pvalue (ptrm_abs T e).

(** ** MCS syntax *)

(** MCS numerals *)
Inductive nvalue : trm -> Prop :=
| nvalue_zero :
    nvalue trm_zero
| nvalue_succ : forall nv,
    nvalue nv ->
    nvalue (trm_succ nv).

(** MCS types *)
Inductive typ : trm -> Prop :=
| typ_nat :
    typ trm_nat
| typ_bool :
    typ trm_bool
| typ_arrow : forall T1 T2,
    typ T1 ->
    typ T2 ->
    typ (trm_arrow T1 T2)
| typ_wedge : forall T1 T2,
    typ T1 ->
    typ T2 ->
    typ (trm_wedge T1 T2)
| typ_refine : forall L T e,
    typ T ->
    (forall x, x \notin L -> exp (e $^ x)) ->
    typ (trm_refine T e)

(** MCS interfaces *)
with interf : trm -> Prop :=
| interf_arrow : forall T1 T2,
    typ (trm_arrow T1 T2) ->
    interf (trm_arrow T1 T2)
| interf_wedge : forall I1 I2,
    interf I1 ->
    interf I2 ->
    interf (trm_wedge I1 I2)

(** MCS expressions *)
with exp : trm -> Prop :=
| exp_zero :
    exp trm_zero
| exp_succ : forall e,
    exp e ->
    exp (trm_succ e)
| exp_pred : forall e,
    exp e ->
    exp (trm_pred e)
| exp_iszero : forall e,
    exp e ->
    exp (trm_iszero e)
| exp_true :
    exp trm_true
| exp_false :
    exp trm_false
| exp_if: forall e1 e2 e3,
    exp e1 ->
    exp e2 ->
    exp e3 ->
    exp (trm_if e1 e2 e3)
| exp_var : forall x,
    exp (trm_fvar x)
| exp_app : forall e1 e2,
    exp e1 ->
    exp e2 ->
    exp (trm_app e1 e2)
| exp_abs : forall L T e,
    typ T ->
    (forall x, x \notin L -> exp (e $^ x)) ->
    exp (trm_abs T e)
| exp_pair : forall e1 e2,
    exp e1 ->
    exp e2 ->
    exp (trm_pair e1 e2)
| exp_fst : forall e,
    exp e ->
    exp (trm_fst e)
| exp_snd : forall e,
    exp e ->
    exp (trm_snd e)
| exp_fix : forall L I B,
    interf I ->
    (forall f, f \notin L -> rbody (B $^ f)) ->
    exp (trm_fix I B)
| exp_cast : forall T1 T2 e,
    exp e ->
    typ T1 ->
    typ T2 ->
    exp (trm_cast e T1 T2)
| exp_delay : forall T T1 T2 v,
    value v ->
    typ T ->
    typ (trm_arrow T1 T2) ->
    exp (trm_delay v T (trm_arrow T1 T2))
| exp_waiting : forall T e1 e2,
    exp e1 ->
    typ (trm_refine T e2) ->
    exp (trm_waiting e1 (trm_refine T e2))
| exp_active : forall T e1 e2 v,
    exp e1 ->
    value v ->
    typ (trm_refine T e2) ->
    exp (trm_active e1 v (trm_refine T e2))

(** MCS recursive function bodies *)
with rbody : trm -> Prop :=
| rbody_abs : forall T e,
    exp (trm_abs T e) ->
    rbody (trm_abs T e)
| rbody_pair : forall B1 B2,
    rbody B1 ->
    rbody B2 ->
    rbody (trm_pair B1 B2)

(** MCS values *)
with value : trm -> Prop :=
| value_zero :
    value trm_zero
| value_succ : forall nv,
    nvalue nv ->
    value (trm_succ nv)
| value_true :
    value trm_true
| value_false :
    value trm_false
| value_abs : forall T e,
    exp (trm_abs T e) ->
    value (trm_abs T e)
| value_pair : forall v1 v2,
    value v1 ->
    value v2 ->
    value (trm_pair v1 v2)
| value_delay : forall T T1 T2 v,
    value v ->
    typ T ->
    typ (trm_arrow T1 T2) ->
    value (trm_delay v T (trm_arrow T1 T2)).

Definition command c := exp c \/ c = trm_blame.

(** MCS type environments *)
Inductive environment : env -> Prop :=
| environment_empty :
    environment empty
| environment_push : forall E x T,
    environment E ->
    x # E ->
    typ T ->
    environment (E & x ~ T).

(** * Semantics *)

Reserved Notation "e -->pcf e'" (at level 68).
Inductive redpcf : ptrm -> ptrm -> Prop :=
| redpcf_beta : forall T e v,
    pexp (ptrm_abs T e) ->
    pvalue v ->
    ptrm_app (ptrm_abs T e) v -->pcf (e ^^ v)%pcf
| redpcf_fix : forall T1 T2 T e,
    pexp (ptrm_fix (ptrm_arrow T1 T2) (ptrm_abs T e)) ->
    ptrm_fix (ptrm_arrow T1 T2) (ptrm_abs T e)
             -->pcf ((ptrm_abs T e) ^^ (ptrm_fix (ptrm_arrow T1 T2) (ptrm_abs T e)))%pcf
| redpcf_pred_zero :
    ptrm_pred ptrm_zero -->pcf ptrm_zero
| redpcf_pred : forall nv,
    pnvalue nv ->
    ptrm_pred (ptrm_succ nv) -->pcf nv
| redpcf_iszero_t :
    ptrm_iszero ptrm_zero -->pcf ptrm_true
| redpcf_iszero_f : forall nv,
    pnvalue nv ->
    ptrm_iszero (ptrm_succ nv) -->pcf ptrm_false
| redpcf_if_t : forall e1 e2,
    pexp e1 ->
    pexp e2 ->
    ptrm_if ptrm_true e1 e2 -->pcf e1
| redpcf_if_f : forall e1 e2,
    pexp e1 ->
    pexp e2 ->
    ptrm_if ptrm_false e1 e2 -->pcf e2
where "e -->pcf e'" := (redpcf e e').

Reserved Notation "e --->pcf e'" (at level 68).
Inductive evalpcf : ptrm -> ptrm -> Prop :=
| evalpcf_redpcf : forall e e',
    e -->pcf e' ->
    e --->pcf e'
| evalpcf_succ : forall e e',
    e --->pcf e' ->
    (ptrm_succ e) --->pcf (ptrm_succ e')
| evalpcf_pred : forall e e',
    e --->pcf e' ->
    (ptrm_pred e) --->pcf (ptrm_pred e')
| evalpcf_iszero : forall e e',
    e --->pcf e' ->
    (ptrm_iszero e) --->pcf (ptrm_iszero e')
| evalpcf_if : forall e1 e1' e2 e3,
    pexp e2 ->
    pexp e3 ->
    e1 --->pcf e1' ->
    (ptrm_if e1 e2 e3) --->pcf (ptrm_if e1' e2 e3)
| evalpcf_app_l : forall e1 e1' e2,
    pexp e2 ->
    e1 --->pcf e1' ->
    (ptrm_app e1 e2) --->pcf (ptrm_app e1' e2)
| evalpcf_app_r : forall e e' v,
    pvalue v ->
    e --->pcf e' ->
    (ptrm_app v e) --->pcf (ptrm_app v e')
where "e --->pcf e'" := (evalpcf e e').

Inductive mevalpcf : ptrm -> ptrm -> Prop :=
| mevalpcf_refl : forall e,
    pexp e ->
    mevalpcf e e
| mevalpcf_eval : forall e e',
    e --->pcf e' ->
    mevalpcf e e'
| mevalpcf_trans : forall e1 e2 e3,
    mevalpcf e1 e2 ->
    mevalpcf e2 e3 ->
    mevalpcf e1 e3.

Reserved Notation "e -->p e'" (at level 68).
Inductive redp : trm -> trm -> Prop :=
| redp_beta : forall T e v,
    exp (trm_abs T e) ->
    value v ->
    (trm_app (trm_abs T e) v) -->p (e ^^ v)
| redp_fix : forall I B,
    exp (trm_fix I B) ->
    trm_fix I B -->p (B ^^ (trm_fix I B))
| redp_pred : forall nv,
    nvalue nv ->
    (trm_pred (trm_succ nv)) -->p nv
| redp_iszero_t :
    (trm_iszero trm_zero) -->p trm_true
| redp_iszero_f : forall nv,
    nvalue nv ->
    (trm_iszero (trm_succ nv)) -->p trm_false
| redp_if_t : forall e1 e2,
    exp e1 ->
    exp e2 ->
    (trm_if trm_true e1 e2) -->p e1
| redp_if_f : forall e1 e2,
    exp e1 ->
    exp e2 ->
    (trm_if trm_false e1 e2) -->p e2
where "e -->p e'" := (redp e e').

Reserved Notation "e --->p e'" (at level 68).
Inductive evalp : trm -> trm -> Prop :=
| evalp_redp : forall e e',
    e -->p e' ->
    e --->p e'
| evalp_succ : forall e e',
    e --->p e' ->
    (trm_succ e) --->p (trm_succ e')
| evalp_pred : forall e e',
    e --->p e' ->
    (trm_pred e) --->p (trm_pred e')
| evalp_iszero : forall e e',
    e --->p e' ->
    (trm_iszero e) --->p (trm_iszero e')
| evalp_if : forall e1 e1' e2 e3,
    exp e2 ->
    exp e3 ->
    e1 --->p e1' ->
    (trm_if e1 e2 e3) --->p (trm_if e1' e2 e3)
| evalp_app_l : forall e1 e1' e2,
    exp e2 ->
    e1 --->p e1' ->
    (trm_app e1 e2) --->p (trm_app e1' e2)
| evalp_app_r : forall e e' v,
    value v ->
    e --->p e' ->
    (trm_app v e) --->p (trm_app v e')
| evalp_fst : forall e e',
    e --->p e' ->
    (trm_fst e) --->p (trm_fst e')
| evalp_snd : forall e e',
    e --->p e' ->
    (trm_snd e) --->p (trm_snd e')
| evalp_cast : forall T1 T2 e e',
    typ T1 ->
    typ T2 ->
    e --->p e' ->
    (trm_cast e T1 T2) --->p (trm_cast e' T1 T2)
| evalp_waiting : forall T e1 e1' e2,
    typ (trm_refine T e2) ->
    e1 --->p e1' ->
    (trm_waiting e1 (trm_refine T e2)) --->p (trm_waiting e1' (trm_refine T e2))
| evalp_pair_s : forall e1 e1' e2 e2',
    e1 --->p e1' ->
    e2 --->p e2' ->
    (trm_pair e1 e2) --->p (trm_pair e1' e2')
where "e --->p e'" := (evalp e e').

Reserved Notation "e -->c e'" (at level 68).
Inductive redc : trm -> trm -> Prop :=
| redc_activate : forall T e v,
    value v ->
    typ (trm_refine T e) ->
    (trm_waiting v (trm_refine T e)) -->c (trm_active (e ^^ v) v (trm_refine T e))
| redc_fst : forall v1 v2,
    value v1 ->
    value v2 ->
    (trm_fst (trm_pair v1 v2)) -->c v1
| redc_snd : forall v1 v2,
    value v1 ->
    value v2 ->
    (trm_snd (trm_pair v1 v2)) -->c v2
| redc_nat: forall v,
    value v ->
    (trm_cast v trm_nat trm_nat) -->c v
| redc_bool : forall v,
    value v ->
    (trm_cast v trm_bool trm_bool) -->c v
| redc_delay : forall T T1 T2 v,
    value v ->
    typ T ->
    typ (trm_arrow T1 T2) ->
    (forall T' e', T <> (trm_refine T' e')) ->
    (trm_cast v T (trm_arrow T1 T2)) -->c (trm_delay v T (trm_arrow T1 T2))
| redc_arrow : forall T11 T12 T21 T22 v1 v2,
    value v1 ->
    value v2 ->
    typ (trm_arrow T11 T12) ->
    typ (trm_arrow T21 T22) ->
    (trm_app (trm_delay v1 (trm_arrow T11 T12) (trm_arrow T21 T22)) v2)
      -->c (trm_cast (trm_app v1 (trm_cast v2 T21 T11)) T12 T22)
| redc_wedge_left : forall T11 T12 T21 T22 v1 v2,
    value v1 ->
    value v2 ->
    typ (trm_wedge T11 T12) ->
    typ (trm_arrow T21 T22) ->
    (trm_app (trm_delay v1 (trm_wedge T11 T12) (trm_arrow T21 T22)) v2)
      -->c (trm_app (trm_cast (trm_fst v1) T11 (trm_arrow T21 T22)) v2)
| redc_wedge_right : forall T11 T12 T21 T22 v1 v2,
    value v1 ->
    value v2 ->
    typ (trm_wedge T11 T12) ->
    typ (trm_arrow T21 T22) ->
    (trm_app (trm_delay v1 (trm_wedge T11 T12) (trm_arrow T21 T22)) v2)
      -->c (trm_app (trm_cast (trm_snd v1) T12 (trm_arrow T21 T22)) v2)
| redc_forget : forall T1 T2 e v,
    value v ->
    typ (trm_refine T1 e) ->
    typ T2 ->
    (trm_cast v (trm_refine T1 e) T2) -->c (trm_cast v T1 T2)
| redc_waiting : forall T1 T2 e v,
    value v ->
    typ T1 ->
    typ (trm_refine T2 e) ->
    (forall T' e', T1 <> (trm_refine T' e')) ->
    (trm_cast v T1 (trm_refine T2 e)) -->c (trm_waiting (trm_cast v T1 T2) (trm_refine T2 e))
| redc_wedge_i : forall T1 T21 T22 v,
    value v ->
    typ T1 ->
    typ (trm_wedge T21 T22) ->
    (forall T' e', T1 <> (trm_refine T' e')) ->
    (trm_cast v T1 (trm_wedge T21 T22)) -->c (trm_pair (trm_cast v T1 T21) (trm_cast v T1 T22))
| redc_wedge_n : forall T11 T12 v,
    value v ->
    typ (trm_wedge T11 T12) ->
    (trm_cast v (trm_wedge T11 T12) trm_nat) -->c (trm_cast (trm_fst v) T11 trm_nat)
| redc_wedge_b : forall T11 T12 v,
    value v ->
    typ (trm_wedge T11 T12) ->
    (trm_cast v (trm_wedge T11 T12) trm_bool) -->c (trm_cast (trm_fst v) T11 trm_bool)
| redc_succeed : forall T e v,
    value v ->
    typ (trm_refine T e) ->
    (trm_active trm_true v (trm_refine T e)) -->c v
| redc_fail : forall T e v,
    value v ->
    typ (trm_refine T e) ->
    (trm_active trm_false v (trm_refine T e)) -->c trm_blame
where "e -->c e'" := (redc e e').

Reserved Notation "e --->c e'" (at level 68).
Inductive evalc : trm -> trm -> Prop :=
| evalc_redc : forall e e',
    e -->c e' ->
    e --->c e'
| evalc_succ : forall e e',
    exp e' ->
    e --->c e' ->
    (trm_succ e) --->c (trm_succ e')
| evalc_pred : forall e e',
    exp e' ->
    e --->c e' ->
    (trm_pred e) --->c (trm_pred e')
| evalc_iszero : forall e e',
    exp e' ->
    e --->c e' ->
    (trm_iszero e) --->c (trm_iszero e')
| evalc_if : forall e1 e1' e2 e3,
    exp e1' ->
    exp e2 ->
    exp e3 ->
    e1 --->c e1' ->
    (trm_if e1 e2 e3) --->c (trm_if e1' e2 e3)
| evalc_app_l : forall e1 e1' e2,
    exp e1' ->
    exp e2 ->
    e1 --->c e1' ->
    (trm_app e1 e2) --->c (trm_app e1' e2)
| evalc_app_r : forall e e' v,
    exp e' ->
    value v ->
    e --->c e' ->
    (trm_app v e) --->c (trm_app v e')
| evalc_fst : forall e e',
    exp e' ->
    e --->c e' ->
    (trm_fst e) --->c (trm_fst e')
| evalc_snd : forall e e',
    exp e' ->
    e --->c e' ->
    (trm_snd e) --->c (trm_snd e')
| evalc_cast : forall T1 T2 e e',
    exp e' ->
    typ T1 ->
    typ T2 ->
    e --->c e' ->
    (trm_cast e T1 T2) --->c (trm_cast e' T1 T2)
| evalc_waiting : forall T e1 e1' e2,
    exp e1' ->
    typ (trm_refine T e2) ->
    e1 --->c e1' ->
    (trm_waiting e1 (trm_refine T e2)) --->c (trm_waiting e1' (trm_refine T e2))
| evalc_active_p : forall T e1 e1' e2 v,
    value v ->
    typ (trm_refine T e2) ->
    e1 --->p e1' ->
    (trm_active e1 v (trm_refine T e2)) --->c (trm_active e1' v (trm_refine T e2))
| evalc_active_c : forall T e1 e1' e2 v,
    exp e1' ->
    value v ->
    typ (trm_refine T e2) ->
    e1 --->c e1' ->
    (trm_active e1 v (trm_refine T e2)) --->c (trm_active e1' v (trm_refine T e2))
| evalc_pair_l : forall e1 e1' e2,
    exp e1' ->
    exp e2 ->
    e1 --->c e1' ->
    (trm_pair e1 e2) --->c (trm_pair e1' e2)
| evalc_pair_r : forall e1 e2 e2',
    exp e2' ->
    exp e1 ->
    e2 --->c e2' ->
    (trm_pair e1 e2) --->c (trm_pair e1 e2')
| evalc_blame_succ : forall e,
    e --->c trm_blame ->
    (trm_succ e) --->c trm_blame
| evalc_blame_pred : forall e,
    e --->c trm_blame ->
    (trm_pred e) --->c trm_blame
| evalc_blame_iszero : forall e,
    e --->c trm_blame ->
    (trm_iszero e) --->c trm_blame
| evalc_blame_if : forall e1 e2 e3,
    exp e2 ->
    exp e3 ->
    e1 --->c trm_blame ->
    (trm_if e1 e2 e3) --->c trm_blame
| evalc_blame_app_l : forall e1 e2,
    exp e2 ->
    e1 --->c trm_blame ->
    (trm_app e1 e2) --->c trm_blame
| evalc_blame_app_r : forall e v,
    value v ->
    e --->c trm_blame ->
    (trm_app v e) --->c trm_blame
| evalc_blame_fst : forall e,
    e --->c trm_blame ->
    (trm_fst e) --->c trm_blame
| evalc_blame_snd : forall e,
    e --->c trm_blame ->
    (trm_snd e) --->c trm_blame
| evalc_blame_cast : forall T1 T2 e,
    typ T1 ->
    typ T2 ->
    e --->c trm_blame ->
    (trm_cast e T1 T2) --->c trm_blame
| evalc_blame_waiting : forall T e1 e2,
    typ (trm_refine T e2) ->
    e1 --->c trm_blame ->
    (trm_waiting e1 (trm_refine T e2)) --->c trm_blame
| evalc_blame_active : forall T e1 e2 v,
    value v ->
    typ (trm_refine T e2) ->
    e1 --->c trm_blame ->
    (trm_active e1 v (trm_refine T e2)) --->c trm_blame
| evalc_blame_pair_l : forall e1 e2,
    exp e2 ->
    e1 --->c trm_blame ->
    (trm_pair e1 e2) --->c trm_blame
| evalc_blame_pair_r : forall e1 e2,
    exp e1 ->
    e2 --->c trm_blame ->
    (trm_pair e1 e2) --->c trm_blame
where "e --->c e'" := (evalc e e').

Reserved Notation "e ---> e'" (at level 68).
Inductive eval : trm -> trm -> Prop :=
| eval_p : forall e e',
    e --->p e' ->
    eval e e'
| eval_c : forall e e',
    e --->c e' ->
    eval e e'
where "e ---> e'" := (eval e e').

Reserved Notation "e --->* e'" (at level 68).
Inductive meval : trm -> trm -> Prop :=
| meval_refl : forall e,
    exp e ->
    meval e e
| meval_eval : forall e e',
    e ---> e' ->
    meval e e'
| meval_trans : forall e1 e2 e3,
    meval e1 e2 ->
    meval e2 e3 ->
    meval e1 e3
where "e --->* e'" := (meval e e').

(** * Type sytem *)

Reserved Notation "E |= e ~: T" (at level 69, e at next level).
Inductive wf_env : env -> Prop :=
| wf_env_empty :
    wf_env empty
| wf_env_push : forall E x T,
    wf_env E ->
    wf_typ T ->
    x # E ->
    wf_env (E & x ~ T)

with wf_typ : trm -> Prop :=
| wf_typ_nat :
    wf_typ trm_nat
| wf_typ_bool :
    wf_typ trm_bool
| wf_typ_arrow : forall T1 T2,
    wf_typ T1 ->
    wf_typ T2 ->
    wf_typ (trm_arrow T1 T2)
| wf_typ_wedge : forall T1 T2,
    wf_typ T1 ->
    wf_typ T2 ->
    essence T1 = essence T2 ->
    wf_typ (trm_wedge T1 T2)
| wf_typ_refine : forall L T e,
    (forall x, x \notin L -> x ~ T |= (e $^ x) ~: trm_bool) ->
    wf_typ (trm_refine T e)

with typing : env -> trm -> trm -> Prop :=
| typing_zero : forall E,
    wf_env E ->
    E |= trm_zero ~: trm_nat
| typing_succ : forall E e,
    E |= e ~: trm_nat ->
    E |= (trm_succ e) ~: trm_nat
| typing_pred : forall E e,
    E |= e ~: (trm_refine trm_nat (trm_if (trm_iszero (trm_bvar 0)) trm_false trm_true)) ->
    E |= (trm_pred e) ~: trm_nat
| typing_iszero : forall E e,
    E |= e ~: trm_nat ->
    E |= (trm_iszero e) ~: trm_bool
| typing_true : forall E,
    wf_env E ->
    E |= trm_true ~: trm_bool
| typing_false : forall E,
    wf_env E ->
    E |= trm_false ~: trm_bool
| typing_if : forall E T e1 e2 e3,
    E |= e1 ~: trm_bool ->
    E |= e2 ~: T ->
    E |= e3 ~: T ->
    E |= (trm_if e1 e2 e3) ~: T
| typing_var : forall E x T,
    wf_env E ->
    binds x T E ->
    E |= (trm_fvar x) ~: T
| typing_abs : forall L E T1 T2 e,
    (forall x, x \notin L -> (E & x ~ T1) |= (e $^ x) ~: T2) ->
    E |= (trm_abs T1 e) ~: (trm_arrow T1 T2)
| typing_app : forall E T1 T2 e1 e2,
    E |= e1 ~: (trm_arrow T1 T2) ->
    E |= e2 ~: T1 ->
    E |= (trm_app e1 e2) ~: T2
| typing_pair : forall E T1 T2 e1 e2,
    E |= e1 ~: T1 ->
    E |= e2 ~: T2 ->
    essence e1 = essence e2 ->
    essence T1 = essence T2 ->
    E |= (trm_pair e1 e2) ~: (trm_wedge T1 T2)
| typing_fst : forall E T1 T2 e,
    E |= e ~: (trm_wedge T1 T2) ->
    E |= (trm_fst e) ~: T1
| typing_snd : forall E T1 T2 e,
    E |= e ~: (trm_wedge T1 T2) ->
    E |= (trm_snd e) ~: T2
| typing_fix : forall L E I B,
    interf I ->
    (forall f, f \notin L -> rbody (B $^ f)) ->
    (forall f, f \notin L -> (E & f ~ I) |= (B $^ f) ~: I) ->
    E |= (trm_fix I B) ~: I
| typing_cast : forall E T1 T2 e,
    E |= e ~: T1 ->
    wf_typ T2 ->
    essence T1 = essence T2 ->
    E |= (trm_cast e T1 T2) ~: T2
| typing_delay : forall E T T1 T2 v,
    value v ->
    wf_env E ->
    empty |= v ~: T ->
    wf_typ (trm_arrow T1 T2) ->
    (forall T' e', T <> (trm_refine T' e')) ->
    essence T = essence (trm_arrow T1 T2) ->
    E |= (trm_delay v T (trm_arrow T1 T2)) ~: (trm_arrow T1 T2)
| typing_waiting : forall E T e1 e2,
    wf_env E ->
    empty |= e1 ~: T ->
    wf_typ (trm_refine T e2) ->
    E |= (trm_waiting e1 (trm_refine T e2)) ~: (trm_refine T e2)
| typing_active : forall E T e1 e2 v,
    value v ->
    wf_env E ->
    empty |= e1 ~: trm_bool ->
    empty |= v ~: T ->
    wf_typ (trm_refine T e2) ->
    (e2 ^^ v) --->* e1 ->
    E |= (trm_active e1 v (trm_refine T e2)) ~: (trm_refine T e2)
| typing_forget : forall E T e v,
    value v ->
    wf_env E ->
    empty |= v ~: (trm_refine T e) ->
    E |= v ~: T
| typing_exact : forall E T e v,
    value v ->
    wf_env E ->
    empty |= v ~: T ->
    wf_typ (trm_refine T e) ->
    (e ^^ v) --->* trm_true ->
    E |= v ~: (trm_refine T e)
where "E |= e ~: T" := (typing E e T).
