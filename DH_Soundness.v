Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.
Require Import ssreflect.
From DH Require Export DH_Regularity.

(** * Determinacy of PCFv evaluation *)

Lemma redpcf_det : forall e e1 e2,
    e -->pcf e1 ->
    e -->pcf e2 ->
    e1 = e2.
Proof. induction 1; inversion 1; auto. Qed.

Lemma pnvalue_is_normal : forall pnv e,
    pnvalue pnv ->
    pnv --->pcf e ->
    False.
Proof.
  introv Pnv. move: e. induction Pnv; intros.
  - inverts H. inverts H0.
  - inverts H.
    + inverts H0.
    + eauto.
Qed.

Lemma pvalue_is_normal : forall pv e,
    pvalue pv ->
    pv --->pcf e ->
    False.
Proof.
  introv Pv. move: e. induction Pv; intros.
  - inverts H; inverts H0.
  - eauto using pnvalue_is_normal.
  - inverts H; inverts H0.
  - inverts H; inverts H0.
  - inverts H0. inverts H1.
Qed.

Lemma evalpcf_det : forall e e1 e2,
    e --->pcf e1 ->
    e --->pcf e2 ->
    e1 = e2.
Proof.
  introv Eval1. move: e2. induction Eval1; inversion 1; subst; f_equal; autos*.
  - eauto using redpcf_det.
  - inverts H.
  - inverts H; false; eauto using pvalue_is_normal.
  - inverts H; false; eauto using pvalue_is_normal.
  - inverts H; false; eauto using pvalue_is_normal.
  - inverts H; false; eauto using pvalue_is_normal.
  - inverts H; false; eauto using pvalue_is_normal.
  - inverts H0.
  - inverts H0; false; eauto using pvalue_is_normal.
  - inverts H0; false; eauto using pvalue_is_normal.
  - inverts H2; false; eauto using pvalue_is_normal.
  - inverts H1; false; eauto using pvalue_is_normal.
  - false; eauto using pvalue_is_normal.
  - false; eauto using pvalue_is_normal.
  - inverts H1; false; eauto using pvalue_is_normal.
  - false; eauto using pvalue_is_normal.
  - false; eauto using pvalue_is_normal.
Qed.

Inductive mevalpcf' : ptrm -> ptrm -> Prop :=
| mevalpcf'_refl : forall e,
    pexp e ->
    mevalpcf' e e
| mevalpcf'_step : forall e1 e2 e3,
    e1 --->pcf e2 ->
    mevalpcf' e2 e3 ->
    mevalpcf' e1 e3.

Lemma mevalpcf'_trans : forall e1 e2 e3,
    mevalpcf' e1 e2 ->
    mevalpcf' e2 e3 ->
    mevalpcf' e1 e3.
Proof.
  induction 1; intros.
  - assumption.
  - apply mevalpcf'_step with (e2 := e2); auto.
Qed.

Lemma mevalpcf'_mevalpcf : forall e e',
    mevalpcf' e e' ->
    mevalpcf e e'.
Proof.
  intros. induction H.
  - apply~ mevalpcf_refl.
  - apply mevalpcf_trans with (e2 := e2).
    + apply~ mevalpcf_eval.
    + assumption.
Qed.

Lemma mevalpcf_mevalpcf' : forall e e',
    mevalpcf e e' ->
    mevalpcf' e e'.
Proof.
  intros. induction H.
  - apply~ mevalpcf'_refl.
  - apply mevalpcf'_step with (e2 := e').
    + assumption.
    + apply~ mevalpcf'_refl. apply (evalpcf_regular_post H).
  - apply mevalpcf'_trans with (e2 := e2); auto.
Qed.

Lemma evalpcf_result_det : forall e v1 v2,
    mevalpcf e v1 ->
    mevalpcf e v2 ->
    pvalue v1 ->
    pvalue v2 ->
    v1 = v2.
Proof.
  intros. apply mevalpcf_mevalpcf' in H. apply mevalpcf_mevalpcf' in H0.
  induction H; inverts H0.
  - by [].
  - false (pvalue_is_normal H1 H3).
  - false (pvalue_is_normal H2 H).
  - rewrite <- (evalpcf_det H H4) in H5. auto.
Qed.

(** * Presupposition of typing *)

Lemma environment_ok : forall E,
    environment E ->
    ok E.
Proof.
  induction 1.
  - by [].
  - by apply ok_push.
Qed.

Lemma wf_env_push_inv : forall E x T,
    wf_env (E & x ~ T) ->
    wf_env E /\ x # E /\ wf_typ T.
Proof.
  inversion 1.
  - false* empty_push_inv.
  - apply eq_push_inv in H0 as (K1 & K2 & K3). by subst.
Qed.

Lemma wf_env_push_inv_E : forall E x T,
    wf_env (E & x ~ T) ->
    wf_env E.
Proof. intros. apply* wf_env_push_inv. Qed.

Lemma wf_env_push_inv_T : forall E x T,
    wf_env (E & x ~ T) ->
    wf_typ T.
Proof. intros. apply* wf_env_push_inv. Qed.

Lemma wf_env_binds_inv : forall E x T,
    wf_env E ->
    binds x T E ->
    wf_typ T.
Proof.
  induction 1; intros.
  - false* binds_empty_inv.
  - binds_cases H2.
    + by auto.
    + by subst.
Qed.

Lemma typing_respect_wf_env : forall E e T,
    E |= e ~: T ->
    wf_env E.
Proof.
  induction 1; auto.
  - specialize_fresh H0. by eauto using wf_env_push_inv_E.
  - specialize_fresh H2. by eauto using wf_env_push_inv_E.
Qed.

Lemma typing_respect_wf_typ : forall E e T,
    E |= e ~: T ->
    wf_typ T.
Proof.
  induction 1; auto.
  - by apply (wf_env_binds_inv H H0).
  - specialize_fresh H. specialize_fresh H0.
    autos* wf_env_push_inv_T typing_respect_wf_env.
  - inverts* IHtyping1.
  - inverts* IHtyping.
  - inverts* IHtyping.
  - specialize_fresh H2. by [].
  - inverts IHtyping. specialize_fresh H3.
    apply_empty wf_env_push_inv_T. rewrite concat_empty_l. by apply (typing_respect_wf_env H2).
Qed.

(** * Weakening lemma *)

Lemma typing_weaken : forall F E G e T,
    (E & F) |= e ~: T ->
    wf_env (E & G & F) ->
    (E & G & F) |= e ~: T.
Proof.
  introv H. gen_eq E': (E & F). move: E F G.
  induction H; introv Eq We; subst*.
  - auto using binds_weaken, environment_ok.
  - apply_fresh typing_abs. apply_ih_bind* H0.
    apply* wf_env_push. forwards~ K: (H y). autos* wf_env_push_inv_T typing_respect_wf_env.
  - apply_fresh~ typing_fix. apply_ih_bind* H2.
    apply* wf_env_push. forwards~ K: (H1 y). autos* wf_env_push_inv_T typing_respect_wf_env.
Qed.

(** * Substitution lemma *)

Lemma fv_open : forall k y x t,
    x <> y -> x \notin (fv_trm ({k ~> trm_fvar y}t)) -> x \notin (fv_trm t).
Proof.
  introv Neq. move: k.
  induction t; simpl; intro; repeat rewrite notin_union; iauto.
Qed.

Definition closed_env x E := forall y T, binds y T E -> x \notin (fv_trm T).

Lemma typing_closed : forall x,
    (forall E, wf_env E -> closed_env x E) /\
    (forall T, wf_typ T -> x \notin (fv_trm T)) /\
    (forall E e T, E |= e ~: T -> closed_env x E /\ (x # E -> x \notin fv_trm e) /\ x \notin (fv_trm T)).
Proof.
  intro x. unfold closed_env. apply wf_env_wf_typ_typing_ind; intros; try (splits 3; intros); simpls*.
  - false* binds_empty_inv.
  - binds_cases H4.
    + by eauto.
    + by subst.
  - specialize_fresh H0. notin_simpl.
    + apply~ (H0 y). apply binds_single_eq.
    + apply* (@fv_open 0 y).
  - rewrite notin_singleton. intro. subst. apply (binds_fresh_inv H1 H2).
  - specialize_fresh H0. eapply H2. apply* binds_push_neq.
  - specialize_fresh H0. notin_simpl.
    + eapply H2. apply* binds_push_eq.
    + apply* (@fv_open 0 y).
  - specialize_fresh H0. notin_simpl.
    + eapply H1. apply* binds_push_eq.
    + apply H1.
  - specialize_fresh H2. eapply H4. apply* binds_push_neq.
  - specialize_fresh H2. notin_simpl.
    + apply* notin_union_r1.
    + apply~ (@fv_open 0 y). autos*.
  - specialize_fresh H2. autos*.
Qed.

Lemma wf_typ_closed : forall x T,
    wf_typ T ->
    x \notin (fv_trm T).
Proof. intros. by apply (proj32 (@typing_closed x)). Qed.

Lemma typing_closed_exp : forall x E e T,
    E |= e ~: T ->
    x # E ->
    x \notin (fv_trm e).
Proof. intros. by apply ((proj33 (@typing_closed x)) E e T). Qed.

Lemma wf_env_remove : forall E F x T,
    wf_env (E & x ~ T & F) ->
    wf_env (E & F).
Proof.
  induction F using env_ind; intros.
  - rew_env_concat in *. by apply (wf_env_push_inv H).
  - rew_env_concat in *. apply wf_env_push.
    + eapply IHF. by apply (wf_env_push_inv H).
    + by apply (wf_env_push_inv H).
    + apply wf_env_push_inv in H as (_ & K & _). auto.
Qed.

Lemma essence_open : forall k u t,
    essence ({k ~> u}t) = ({k ~> essence u}(essence t))%pcf.
Proof. intros. move: k. induction t; simpl; intros; f_equal; auto. by calc_open_subst. Qed.

Lemma essence_subst : forall x u t,
    essence ([x ~> u]t) = ([x ~> essence u](essence t))%pcf.
Proof. induction t; simpl; f_equal; auto. by calc_open_subst. Qed.

Lemma typing_subst : forall F E x T T' u e,
    (E & x ~ T' & F) |= e ~: T ->
    E |= u ~: T' ->
    (E & F) |= [x ~> u]e ~: T.
Proof.
  introv H. gen_eq E': (E & x ~ T' & F). move: E F x T' u.
  induction H; introv Eq Typ; subst; simpl; try solve [ autos* wf_env_remove ].
  - calc_open_subst.
    + binds_get H0.
      * autos* environment_ok.
      * apply_empty* typing_weaken. apply (wf_env_remove H).
    + binds_cases H0; autos* wf_env_remove.
  - rewrite subst_fresh_trm.
    + specialize_fresh H. autos* wf_typ_closed wf_env_push_inv_T typing_respect_wf_env.
    + apply_fresh typing_abs. rewrite~ <- subst_open_var_trm. apply_ih_bind* H0.
  - apply* typing_pair. do 2 rewrite essence_subst. congruence.
  - rewrite subst_fresh_trm.
    + specialize_fresh H1. autos* wf_typ_closed wf_env_push_inv_T typing_respect_wf_env.
    + apply_fresh~ typing_fix.
      * rewrite~ <- subst_open_var_trm.
      * rewrite~ <- subst_open_var_trm. apply_ih_bind* H2.
  - rewrite [_ T1] subst_fresh_trm; first by autos* wf_typ_closed typing_respect_wf_typ.
    rewrite [subst_trm _ _ T2] subst_fresh_trm; first by autos* wf_typ_closed.
    apply* typing_cast.
  - replace (trm_arrow ([x ~> u]T1) ([x ~> u]T2)) with ([x ~> u](trm_arrow T1 T2)) by reflexivity.
    rewrite [[x ~> u](trm_arrow _ _)] subst_fresh_trm; first by apply* wf_typ_closed.
    rewrite [_ T] subst_fresh_trm; first by eauto using wf_typ_closed, typing_respect_wf_typ.
    rewrite [_ v] subst_fresh_trm; first by apply* typing_closed_exp.
    apply* typing_delay. by apply (wf_env_remove H0).
  - replace (trm_refine ([x ~> u]T) ([x ~> u]e2)) with ([x ~> u](trm_refine T e2)) by reflexivity.
    rewrite [[x ~> u](trm_refine _ _)] subst_fresh_trm; first by apply* wf_typ_closed.
    rewrite [_ e1] subst_fresh_trm; first by apply* typing_closed_exp.
    apply* typing_waiting. by apply (wf_env_remove H).
  - replace (trm_refine ([x ~> u]T) ([x ~> u]e2)) with ([x ~> u](trm_refine T e2)) by reflexivity.
    rewrite [[x ~> u](trm_refine T e2)] subst_fresh_trm; first by apply* wf_typ_closed.
    rewrite [_ e1] subst_fresh_trm; first by apply* typing_closed_exp.
    rewrite [_ v] subst_fresh_trm; first by apply* typing_closed_exp.
    apply* typing_active. by apply (wf_env_remove H0).
  - rewrite subst_fresh_trm; first by apply* typing_closed_exp.
    apply* typing_forget. apply (wf_env_remove H0).
  - rewrite subst_fresh_trm; first by apply* typing_closed_exp.
    apply* typing_exact. apply (wf_env_remove H0).
Qed.

(** * Typing inversion *)

Ltac typing_inversion H :=
  match type of H with
  | _ |= ?e ~: _ =>
    inversion H;
    try solve [ match goal with [ H: value e |- _ ] => inversion H end ]
  end.

Inductive refined : trm -> trm -> Prop :=
| refined_refl : forall T,
    refined T T
| refined_left : forall T1 T2 e,
    refined T1 T2 ->
    refined (trm_refine T1 e) T2
| refined_right : forall T1 T2 e,
    refined T1 T2 ->
    refined T1 (trm_refine T2 e).

Hint Constructors refined.

Lemma refined_inv : forall T1 T2 e,
    refined T1 (trm_refine T2 e) ->
    refined T1 T2.
Proof.
  introv H. gen_eq: (trm_refine T2 e). gen T2 e. induction H; introv Eq; subst.
  - apply refined_left. apply refined_refl.
  - apply* refined_left.
  - congruence.
Qed.

Lemma typing_zero_inv : forall T,
    empty |= trm_zero ~: T ->
    refined trm_nat T.
Proof.
  introv Typ. gen_eq e': trm_zero. induction Typ; introv Eq; tryfalse; subst*.
  - eapply refined_inv. iauto.
Qed.

Lemma typing_succ_inv : forall e T,
    empty |= (trm_succ e) ~: T ->
    empty |= e ~: trm_nat /\ refined trm_nat T.
Proof.
  introv Typ. gen_eq E: (empty : env). gen_eq e': (trm_succ e). gen e.
  induction Typ; introv Eqe EqE; subst E; tryfalse; subst*.
  - inverts Eqe. auto using refined_refl.
  - split.
    + apply~ IHTyp.
    + eapply refined_inv. apply~ IHTyp.
  - split.
    + apply~ IHTyp.
    + apply refined_right. apply~ IHTyp.
Qed.

Lemma typing_true_inv : forall T,
    empty |= trm_true ~: T ->
    refined trm_bool T.
Proof.
  introv Typ. gen_eq e': trm_true. induction Typ; introv Eq; tryfalse; subst*.
  - eapply refined_inv. iauto.
Qed.

Lemma typing_false_inv : forall T,
    empty |= trm_false ~: T ->
    refined trm_bool T.
Proof.
  introv Typ. gen_eq e': trm_false. induction Typ; introv Eq; tryfalse; subst*.
  - eapply refined_inv. iauto.
Qed.

Lemma typing_add_env : forall E e T,
    wf_env E ->
    empty |= e ~: T ->
    E |= e ~: T.
Proof. intros. move: (@typing_weaken empty empty E e T) => K. rew_env_concat in K. auto. Qed.

Lemma typing_pair_inv_gen : forall E e1 e2 T,
    E |= (trm_pair e1 e2) ~: T ->
    exists T1 T2, (E |= e1 ~: T1) /\ (E |= e2 ~: T2) /\
             (essence e1 = essence e2) /\
             (essence T1 = essence T2) /\
             (refined (trm_wedge T1 T2) T).
Proof.
  introv Typ. gen_eq e': (trm_pair e1 e2). gen e1 e2.
  induction Typ; introv Eqe; tryfalse; subst*.
  - inverts Eqe. exists T1 T2. splits* 5.
  - forwards*: IHTyp. move: H1 => [T1 [T2]]. exists T1 T2. splits* 5.
    + by intuition auto using typing_add_env.
    + by intuition auto using typing_add_env.
    + by apply* refined_inv.
  - forwards*: IHTyp. move: H3 => [T1 [T2]]. exists T1 T2. splits* 5.
    + by intuition auto using typing_add_env.
    + by intuition auto using typing_add_env.
Qed.

Lemma typing_pair_inv_fst : forall E e1 e2 T1 T2,
    E |= (trm_pair e1 e2) ~: (trm_wedge T1 T2) ->
    E |= e1 ~: T1.
Proof.
  introv Typ. apply typing_pair_inv_gen in Typ.
  destruct Typ as (? & ? & ? & ? & ? & ? & Eq). inversion Eq. subst. assumption.
Qed.

Lemma typing_pair_inv_snd : forall E e1 e2 T1 T2,
    E |= (trm_pair e1 e2) ~: (trm_wedge T1 T2) ->
    E |= e2 ~: T2.
Proof.
  introv Typ. apply typing_pair_inv_gen in Typ.
  destruct Typ as (? & ? & ? & ? & ? & ? & Eq). inversion Eq. subst. assumption.
Qed.

Lemma typing_abs_inv_gen : forall E T1 T2 e,
    E |= (trm_abs T1 e) ~: T2 ->
    exists T1' T2', (exists L, forall x, x \notin L -> E & x ~ T1' |= (e $^ x) ~: T2') /\
               (refined (trm_arrow T1' T2') T2).
Proof.
  introv Typ. gen_eq e': (trm_abs T1 e). gen T1 e.
  induction Typ; introv Eqe; tryfalse; subst*.
  - inverts Eqe. exists T0 T2. split*.
  - forwards~ : IHTyp. move: H1 => [T1' [T2' [[L K1] K2]]].
    exists T1' T2'. split.
    + exists (L \u dom E). intros. forwards~ : (K1 x).
      assert (Wf: wf_env (empty & E & x ~ T1')).
      { rew_env_concat. apply~ wf_env_push. apply_empty wf_env_push_inv_T.
          by apply (typing_respect_wf_env H2). }
      rewrite <- (@concat_empty_l trm (E & x ~ T1')).
      rewrite concat_assoc. by apply typing_weaken.
    + by apply (refined_inv K2).
  - forwards~ : IHTyp. move: H3 => [T1' [T2' [[L K1] K2]]].
    exists T1' T2'. split.
    + exists (L \u dom E). intros. forwards~ : (K1 x).
      assert (Wf: wf_env (empty & E & x ~ T1')).
      { rew_env_concat. apply~ wf_env_push. apply_empty wf_env_push_inv_T.
          by apply (typing_respect_wf_env H4). }
      rewrite <- (@concat_empty_l trm (E & x ~ T1')).
      rewrite concat_assoc. by apply typing_weaken.
    + by apply refined_right.
Qed.

Lemma typing_abs_inv : forall E T T1 T2 e,
    E |= (trm_abs T e) ~: (trm_arrow T1 T2) ->
    exists L, forall x, x \notin L -> E & x ~ T1 |= (e $^ x) ~: T2.
Proof.
  intros. move: (typing_abs_inv_gen H) => [T1' [T2' [[L K1] K2]]]. exists L. by inverts K2.
Qed.

Lemma typing_fix_inv : forall E T I B,
    E |= trm_fix I B ~: T ->
    (exists L, (forall f, f \notin L -> rbody (B $^ f)) /\
          (forall f, f \notin L -> E & f ~ I |= B $^ f ~: I)) /\
    (interf I) /\ (refined I T).
Proof.
  introv Typ. gen_eq e': (trm_fix I B). gen I B.
  induction Typ; introv Eqe; tryfalse; subst*.
  - inverts* Eqe.
  - forwards~ K: IHTyp. move: K => [[L [K1 K2]] K3]. split.
    + exists (L \u dom E). split; intros; auto. forwards~ K4: (K2 f).
      assert (Wf: wf_env (empty & E & f ~ I)).
      { rew_env_concat. apply~ wf_env_push. apply_empty wf_env_push_inv_T.
          by apply (typing_respect_wf_env K4). }
      rewrite <- (@concat_empty_l trm (E & f ~I)).
      rewrite concat_assoc. by apply typing_weaken.
    + intuition eauto using refined_inv.
  - forwards~ K: IHTyp. move: K => [[L [K1 K2]] K3]. split.
    + exists (L \u dom E). split; intros; auto. forwards~ K4: (K2 f).
      assert (Wf: wf_env (empty & E & f ~ I)).
      { rew_env_concat. apply~ wf_env_push. apply_empty wf_env_push_inv_T.
          by apply (typing_respect_wf_env K4). }
      rewrite <- (@concat_empty_l trm (E & f ~I)).
      rewrite concat_assoc. by apply typing_weaken.
    + intuition eauto using refined_right.
Qed.

Lemma typing_delay_inv_gen : forall T T' T1 T2 v,
    empty |= trm_delay v T (trm_arrow T1 T2) ~: T' ->
    (empty |= v ~: T) /\
    (wf_typ (trm_arrow T1 T2)) /\
    (forall T' e', T <> (trm_refine T' e')) /\
    (essence T = essence (trm_arrow T1 T2)) /\
    (refined (trm_arrow T1 T2) T').
Proof.
  introv Typing. gen_eq E: (empty : env). gen_eq e': (trm_delay v T (trm_arrow T1 T2)). gen T T1 T2 v.
  induction Typing; introv Eqe EqE; tryfalse; subst.
  - inverts Eqe. splits* 5.
  - forwards~ : IHTyping. splits* 5. by intuition eauto using refined_inv.
  - forwards~ : IHTyping. splits* 5.
Qed.

Lemma typing_delay_inv : forall T T1 T2 T1' T2' v,
    empty |= trm_delay v T (trm_arrow T1 T2) ~: trm_arrow T1' T2' ->
    T1 = T1' /\ T2 = T2'.
Proof. intros. move: (typing_delay_inv_gen H) => [_ [_ [_ [_ K]]]]. inverts* K. Qed.

Hint Extern 10 (wf_env ?E) =>
match goal with
| [ H: E |= _ ~: _ |- _ ] => exact (typing_respect_wf_env H)
end.

Hint Extern 10 (wf_typ ?T) =>
match goal with
| [ H: _ |= _ ~: T |- _ ] => exact (typing_respect_wf_typ H)
end.

(** * Simulations between evaluations *)

Lemma redp_redpcf : forall T e e',
    e -->p e' ->
    empty |= e ~: T ->
    essence e -->pcf essence e'.
Proof.
  induction 1; simpl; intros; auto.
  - rewrite essence_open. apply~ redpcf_beta. apply (essence_exp H).
  - rewrite! essence_open. simpl.
    move: (typing_fix_inv H0) => [[L [K1 K2]] [K3 K4]].
    pick_fresh f. forwards~ K1': (K1 f). forwards~ K2': (K2 f).
    move: (essence_interf K3) => [T1 [T2 K3']].
    rewrite -(@close_var_open_trm f B); first by auto.
    rewrite essence_close_var.
    move: (essence_rbody K1') => [T' [e' K1'']].
    rewrite K3' K1'' /close_var_ptrm /=.
    apply redpcf_fix.
    apply_fresh~ pexp_fix.
    + rewrite -K3'. auto.
    + rewrite -[ptrm_abs _ _]/(close_var_ptrm f (ptrm_abs T' e')).
      apply close_var_open_pexp. rewrite -K1''. auto.
Qed.

Lemma evalp_evalpcf : forall T e e',
    e --->p e' ->
    empty |= e ~: T ->
    essence e --->pcf essence e'.
Proof.
  introv Eval. move: T.
  induction Eval; simpl; intros; try eauto using redp_redpcf;
    try solve [ match goal with
                | [ H: empty |= ?e ~: _ |- _ ] =>
                  let E := fresh in
                  let e' := fresh in
                  gen_eq E: (empty : env); gen_eq e': e; induction H;
                  let Eq1 := fresh in
                  let Eq2 := fresh in
                  intros Eq1 Eq2; inverts Eq1; subst; eauto
                end ].
Qed.

Lemma evalp_sync : forall T1 T2 e1 e2 e1' e2',
    e1 --->p e1' ->
    e2 --->p e2' ->
    empty |= e1 ~: T1 ->
    empty |= e2 ~: T2 ->
    essence e1 = essence e2 ->
    essence e1' = essence e2'.
Proof.
  intros. eapply evalpcf_det.
  - apply (evalp_evalpcf H H1).
  - rewrite H3. apply (evalp_evalpcf H0 H2).
Qed.

Lemma redc_preserve_essence : forall T e e',
    empty |= e ~: T ->
    e -->c e' ->
    exp e' ->
    essence e = essence e'.
Proof.
  introv Typing Redc Exp.
  inverts Redc; tryfalse; simpl; auto.
  - typing_inversion Typing. subst.
    move: (typing_pair_inv_gen H3) => [_ [_ [_ [_ [-> _]]]]]. auto.
  - inverts Exp.
Qed.

Lemma evalc_preserve_essence : forall T e e',
    empty |= e ~: T ->
    e --->c e' ->
    exp e' ->
    essence e = essence e'.
Proof.
  introv Typing Evalc Exp. move: T Typing.
  induction Evalc; tryfalse; simpl in *; intros; auto;
    try solve [ inverts Exp ];
    try solve [ typing_inversion Typing; f_equal; autos* ];
    try solve [ typing_inversion Typing; autos* ].
  - by eauto using redc_preserve_essence.
  - move: (typing_succ_inv Typing) => [K _]. f_equal. apply* IHEvalc.
  - move: (typing_pair_inv_gen Typing) => [T1 [_ [K _]]]. autos*.
Qed.

Lemma non_destructive_testing : forall T e e',
    empty |= e ~: T ->
    e ---> e' ->
    exp e' ->
    mevalpcf (essence e) (essence e').
Proof.
  intros. inverts H0.
  - apply mevalpcf_eval. apply* evalp_evalpcf.
  - rewrite <- (evalc_preserve_essence H H2 H1). apply* mevalpcf_refl.
Qed.

(** * Preservation lemma *)

Lemma redp_preserve_typing : forall e e' T,
    empty |= e ~: T ->
    e -->p e' ->
    empty |= e' ~: T.
Proof.
  introv Typing. gen_eq E: (empty : env). move: e'.
  induction Typing; introv Eq Redp; subst;
    try solve [ match goal with [ H1: value ?v, H2: ?v -->p _ |- _ ] => inverts H2; inverts H1 end ];
    inverts Redp; auto.
  - apply* typing_succ_inv.
  - move: (typing_abs_inv Typing1) => [L K]. specialize_fresh K.
    rewrite~ (@subst_intro_trm y). by apply_empty~ typing_subst.
  - specialize_fresh H1. rewrite~ (@subst_intro_trm y). apply_empty~ typing_subst.
Qed.

Lemma redc_preserve_typing : forall e e' T,
    empty |= e ~: T ->
    e -->c e' ->
    exp e' ->
    empty |= e' ~: T.
Proof.
  introv Typing. gen_eq E: (empty : env). move: e'.
  induction Typing; introv Eq Redc Exp; subst;
    try solve [ match goal with [ H1: value ?v, H2: ?v -->c _ |- _ ] => inverts H2; inverts H1 end ];
    inverts Redc; auto.
  - move: (typing_delay_inv_gen Typing1) => [K1 [K2 [K3 [K4 K5]]]].
    move: (typing_respect_wf_typ K1) => K6.
    inverts K2. simpl in K4. inverts K5. inverts K6.
    apply* typing_cast.
    + apply* typing_app. apply* typing_cast. congruence.
    + congruence.
  - move: (typing_delay_inv_gen Typing1) => [K1 [K2 [K3 [K4 K5]]]].
    move: (typing_respect_wf_typ K1) => K6.
    inverts K2. simpl in K4. inverts K5. inverts K6.
    apply* typing_app.
  - move: (typing_delay_inv_gen Typing1) => [K1 [K2 [K3 [K4 K5]]]].
    move: (typing_respect_wf_typ K1) => K6.
    inverts K2. simpl in K4. inverts K5. inverts K6.
    apply* typing_app. apply* typing_cast. simpl. congruence.
  - by eauto using typing_pair_inv_fst.
  - by eauto using typing_pair_inv_snd.
  - eauto.
  - simpl in H0. inverts H.
    apply~ typing_waiting. apply~ typing_cast.
    specialize_fresh H2. apply_empty wf_env_push_inv_T. rew_env_concat.
    by eauto using typing_respect_wf_env.
  - inverts H. apply~ typing_pair.
    + apply~ typing_cast. simpl in H0. congruence.
  - eauto.
  - eauto.
  - apply~ typing_active.
    + inverts H0. specialize_fresh H2. rewrite~ (@subst_intro_trm y).
      move: (@typing_subst empty empty y trm_bool T e1) => K. rew_env_concat in K. auto.
    + apply meval_refl. inverts H6. pick_fresh y. rewrite~ (@subst_intro_trm y).
  - inverts Exp.
Qed.

Lemma nvalue_is_normal_evalp : forall nv e,
    nvalue nv ->
    nv --->p e ->
    False.
Proof.
  introv Nval. move: e. induction Nval; intros.
  - inverts H. inverts H0.
  - inverts* H. inverts H0.
Qed.

Lemma value_is_normal_evalp : forall v e,
    value v ->
    v --->p e ->
    False.
Proof.
  introv Nval. move: e. induction Nval; intros.
  - inverts H. inverts H0.
  - inverts H0.
    + inverts H1.
    + apply* nvalue_is_normal_evalp.
  - inverts H. inverts H0.
  - inverts H. inverts H0.
  - inverts H0. inverts H1.
  - inverts* H. inverts H0.
  - inverts H1. inverts H2.
Qed.

Lemma evalp_preserve_typing : forall e e' T,
    empty |= e ~: T ->
    e --->p e' ->
    empty |= e' ~: T.
Proof.
  introv Typing. gen_eq E: (empty : env). assert (E |= e ~: T) by easy. move: e' H.
  induction Typing; introv Typing' Eq Evalp; subst;
    try solve [ match goal with [ H1: value ?v, H2: ?v --->p _ |- _ ] => false (value_is_normal_evalp H1 H2) end ];
    (inverts Evalp; first by apply (redp_preserve_typing Typing')); eauto.
  - apply* typing_pair. by apply (evalp_sync H3 H5 Typing1 Typing2).
Qed.

Lemma nvalue_is_normal_evalc : forall nv e,
    nvalue nv ->
    nv --->c e ->
    False.
Proof.
  introv Nval. move: e. induction Nval; intros.
  - inverts H. inverts H0.
  - inverts* H. inverts H0.
Qed.

Lemma value_is_normal_evalc : forall v e,
    value v ->
    v --->c e ->
    False.
Proof.
  introv Nval. move: e. induction Nval; intros.
  - inverts H. inverts H0.
  - inverts H0.
    + inverts H1.
    + apply* nvalue_is_normal_evalc.
    + apply* nvalue_is_normal_evalc.
  - inverts H. inverts H0.
  - inverts H. inverts H0.
  - inverts H0. inverts H1.
  - inverts* H. inverts H0.
  - inverts H1. inverts H2.
Qed.

Lemma evalc_preserve_typing : forall e e' T,
    empty |= e ~: T ->
    e --->c e' ->
    exp e' ->
    empty |= e' ~: T.
Proof.
  introv Typing. gen_eq E: (empty : env). assert (E |= e ~: T) by easy. move: e' H.
  induction Typing; introv Typing' Eq Evalc Exp; subst;
    try solve [ match goal with [ H1: value ?v, H2: ?v --->c _ |- _ ] => false (value_is_normal_evalc H1 H2) end ];
    (inverts Evalc; first by apply (redc_preserve_typing Typing')); try solve [ inverts Exp ]; eauto.
  - apply* typing_pair. replace (essence e1') with (essence e1); first easy.
    by apply* evalc_preserve_essence.
  - apply* typing_pair. replace (essence e2') with (essence e2); first easy.
    by apply* evalc_preserve_essence.
  - apply* typing_active.
    + apply (evalp_preserve_typing Typing1 H10).
    + apply* meval_trans. apply* meval_eval.
  - apply* typing_active. apply* meval_trans. apply* meval_eval.
Qed.

Lemma preservation : forall e e' T,
    empty |= e ~: T ->
    e ---> e' ->
    exp e' ->
    empty |= e' ~: T.
Proof.
  intros. inverts H0.
  - by eauto using evalp_preserve_typing.
  - by eauto using evalc_preserve_typing.
Qed.

(** * Value inversion *)

Inductive satisfy_predicate : trm -> trm -> Prop :=
| sp_nat : forall v,
    satisfy_predicate v trm_nat
| sp_bool : forall v,
    satisfy_predicate v trm_bool
| sp_arrow : forall v T1 T2,
    satisfy_predicate v (trm_arrow T1 T2)
| sp_refine : forall v T e,
    satisfy_predicate v T ->
    e ^^ v --->* trm_true ->
    satisfy_predicate v (trm_refine T e)
| sp_wedge : forall v T1 T2,
    satisfy_predicate v (trm_wedge T1 T2).

Lemma satisfy_predicate_refine_inv : forall v T e,
    satisfy_predicate v (trm_refine T e) -> satisfy_predicate v T /\ e ^^ v --->* trm_true.
Proof. inversion 1. auto. Qed.

Lemma value_inversion_gen : forall T v,
    value v ->
    empty |= v ~: T ->
    satisfy_predicate v T.
Proof.
  introv Val Typing. gen_eq E: (empty : env).
  induction Typing; intro Eq; subst; try solve [ inverts Val ]; auto using satisfy_predicate.
  - eapply satisfy_predicate_refine_inv. iauto.
Qed.

Lemma value_inversion : forall T e v,
    value v ->
    empty |= v ~: (trm_refine T e) ->
    e ^^ v --->* trm_true.
Proof. intros. eapply satisfy_predicate_refine_inv. eauto using value_inversion_gen. Qed.

(** * Progress lemma *)

Lemma canonical_bool : forall E v T,
    E |= v ~: T ->
    E = empty ->
    value v ->
    refined trm_bool T ->
    v = trm_true \/ v = trm_false.
Proof.
  induction 1; introv Eem Val Ref; subst;
    auto using refined;
    inverts~ Ref;
    inverts~ Val.
Qed.

Lemma canonical_nat : forall E v T,
    E |= v ~: T ->
    E = empty ->
    value v ->
    refined trm_nat T ->
    nvalue v.
Proof.
  induction 1; introv Eem Val Ref; subst;
    auto using refined;
    inverts~ Ref;
    inverts~ Val.
Qed.

Lemma canonical_arrow' : forall E v T T1 T2,
    E |= v ~: T ->
    E = empty ->
    value v ->
    refined (trm_arrow T1 T2) T ->
    (exists T e, v = trm_abs T e) \/
    (exists v' T11 T12 T21 T22, v = trm_delay v' (trm_arrow T11 T12) (trm_arrow T21 T22)) \/
    (exists v' T11 T12 T21 T22, v = trm_delay v' (trm_wedge T11 T12) (trm_arrow T21 T22)).
Proof.
  induction 1; introv Eem Val Ref; subst;
    auto using refined;
    inverts~ Ref;
    inverts~ Val.
  - eauto.
  - inverts H10; inverts H4.
    + right. left. jauto.
    + right. right. jauto.
    + false~ H3.
Qed.

Lemma canonical_arrow : forall v T1 T2,
    empty |= v ~: trm_arrow T1 T2 ->
    value v ->
    (exists T e, v = trm_abs T e) \/
    (exists v' T11 T12 T21 T22, v = trm_delay v' (trm_arrow T11 T12) (trm_arrow T21 T22)) \/
    (exists v' T11 T12 T21 T22, v = trm_delay v' (trm_wedge T11 T12) (trm_arrow T21 T22)).
Proof. intros. intros. forwards* : canonical_arrow'. Qed.

Lemma canonical_wedge' : forall E v T T1 T2,
    E |= v ~: T ->
    E = empty ->
    value v ->
    refined (trm_wedge T1 T2) T ->
    exists v1 v2, v = trm_pair v1 v2.
Proof.
  induction 1; introv Eem Val Ref; subst;
    auto using refined;
    inverts~ Ref;
    inverts~ Val.
  - eauto.
Qed.

Lemma canonical_wedge : forall v T1 T2,
    empty |= v ~: (trm_wedge T1 T2) ->
    value v ->
    exists v1 v2, v = trm_pair v1 v2.
Proof. intros. forwards* : canonical_wedge'. Qed.

Lemma prsv_meval : forall T e e',
    empty |= e ~: T ->
    e --->* e' ->
    exp e' ->
    empty |= e' ~: T.
Proof.
  induction 2; intros; auto.
  - apply (preservation H H0 H1).
Qed.

Lemma eval_evalpcf : forall T e e',
    empty |= e ~: T ->
    e --->* e' ->
    exp e' ->
    mevalpcf (essence e) (essence e').
Proof.
  induction 2; intros.
  - apply~ mevalpcf_refl.
  - apply (non_destructive_testing H H0 H1).
  - apply mevalpcf_trans with (e2 := essence e2).
    + auto.
    + apply~ IHmeval2. apply~ (prsv_meval H H0_).
Qed.

Lemma progress_result : forall e T,
    empty |= e ~: T ->
    value e \/ exists e', e ---> e'.
Proof.
  introv Typing. gen_eq E: (empty : env). induction Typing; introv Eq; subst; auto.
  - forwards~ K: IHTyping. destruct K.
    + left. apply value_succ. apply~ (canonical_nat Typing).
    + right. unpack. inverts H.
      * exists~ (trm_succ e').
      * destruct (evalc_regular_post H0); subst; eauto.
  - forwards~ K: IHTyping. destruct K.
    + right. forwards~ K: (canonical_nat Typing). inverts K.
      * move: (value_inversion H Typing) => K. unfold open_trm in K. simpl in K. case_nat.
        assert (K': trm_if (trm_iszero trm_zero) trm_false trm_true --->* trm_false).
        { apply meval_trans with (e2 := trm_if trm_true trm_false trm_true); apply meval_eval; auto. }
        eapply eval_evalpcf in K; auto. eapply eval_evalpcf in K'; auto.
        false~ (evalpcf_result_det K K').
      * eauto.
    + right. unpack. inverts H.
      * eauto.
      * destruct (evalc_regular_post H0); subst; eauto.
  - forwards~ K: IHTyping. destruct K.
    + right. forwards~ K: (canonical_nat Typing). inverts K; eauto.
    + right. unpack. inverts H.
      * eauto.
      * destruct (evalc_regular_post H0); subst; eauto.
  - forwards~ K: IHTyping1. destruct K.
    + right. forwards~ K: (canonical_bool Typing1). inverts K; eauto.
    + right. unpack. inverts H.
      * eauto.
      * destruct (evalc_regular_post H0); subst; eauto.
  - false~ (binds_empty_inv H0).
  - left. apply value_abs. apply* typing_regular_e.
  - forwards~ K: IHTyping1. destruct K.
    + right. forwards~ K1: IHTyping1. forwards~ K2: IHTyping2. destruct K1.
      * destruct K2.
        -- destruct (canonical_arrow Typing1 H) as [K | [K | K]].
           ++ unpack. subst. exists (e ^^ e2). auto.
           ++ unpack. subst. exists (trm_cast (trm_app v' (trm_cast e2 T21 T11)) T12 T22).
              inverts* H.
           ++ unpack. subst. exists (trm_app (trm_cast (trm_fst v') T11 (trm_arrow T21 T22)) e2).
              inverts* H.
        -- unpack H1. inverts TEMP.
           ++ eauto.
           ++ destruct (evalc_regular_post H1); subst; eauto.
      * unpack H0. inverts TEMP.
        ++ eauto.
        ++ destruct (evalc_regular_post H0); subst; eauto.
    + right. unpack. inverts H.
      * eauto.
      * destruct (evalc_regular_post H0); subst; eauto.
  - forwards~ K1: IHTyping1. forwards~ K2: IHTyping2. destruct K1.
    + destruct K2.
      * left. auto.
      * right. unpack H2. inverts TEMP.
        -- move: (evalp_evalpcf H2 Typing2) => K. rewrite <- H in K. false.
           refine (pvalue_is_normal _ K). auto.
        -- destruct (evalc_regular_post H2); subst; eauto.
    + right. unpack H1. inverts TEMP.
      -- destruct K2.
         ++ move: (evalp_evalpcf H1 Typing1) => K. rewrite H in K. false.
            refine (pvalue_is_normal _ K). auto.
         ++ unpack H2. inverts TEMP.
            ** exists (trm_pair e' e'0). auto.
            ** destruct (evalc_regular_post H2); subst; eauto.
      -- destruct (evalc_regular_post H1); subst; eauto.
  - forwards~ K: IHTyping. destruct K.
    + right. destruct (canonical_wedge Typing H); unpack; subst.
      * inverts H. exists x. auto.
    + right. unpack. inverts H.
      * eauto.
      * destruct (evalc_regular_post H0); subst; eauto.
  - forwards~ K: IHTyping. destruct K.
    + right. destruct (canonical_wedge Typing H); unpack; subst.
      * inverts H. exists v2. auto.
    + right. unpack. inverts H.
      * eauto.
      * destruct (evalc_regular_post H0); subst; eauto.
  - right. assert (exp (trm_fix I B)) by auto. eauto.
  - forwards~ K: IHTyping. destruct K.
    + right. assert (K1: typ T1) by auto. assert (K2: typ T2) by auto.
      inverts K1; inverts K2; simpl in H0; try discriminate;
        eexists; apply eval_c; apply evalc_redc; eauto.
      * apply* redc_wedge_i. discriminate.
      * apply* redc_waiting. discriminate.
      * apply* redc_wedge_i. discriminate.
      * apply* redc_waiting. discriminate.
      * apply* redc_delay. discriminate.
      * apply* redc_wedge_i. discriminate.
      * apply* redc_waiting. discriminate.
      * apply* redc_delay. discriminate.
      * apply* redc_wedge_i. discriminate.
      * apply* redc_waiting. discriminate.
    + right. unpack. inverts H1.
      * eauto.
      * destruct (evalc_regular_post H2); subst; eauto.
  - forwards~ K: IHTyping. destruct K.
    + right. exists (trm_active (e2 ^^ e1) e1 (trm_refine T e2)). auto.
    + right. unpack. inverts H1.
      * eauto.
      * destruct (evalc_regular_post H2); subst; eauto.
  - forwards~ K: IHTyping1. destruct K.
    + right. forwards~ K: (canonical_bool Typing1). destruct K; subst.
      * exists v. auto.
      * exists trm_blame. auto.
    + right. unpack. inverts H3.
      * eauto.
      * destruct (evalc_regular_post H4); subst; eauto.
Qed.
